﻿namespace = marcher_lich

marcher_lich.0001 = {
	title = marcher_lich.0001.t
	desc = marcher_lich.0001.desc
	theme = battle
	override_background = {
		event_background = lich_raid
	}
	left_portrait = { 
		character = root
	}

	option = {
		name = marcher_lich.0001.a
	}
	option = {
		name = marcher_lich.0001.b
		trigger = {
			stewardship >= 12
			is_available_adult_or_is_commanding = yes
		}
	}
	option = {
		name = marcher_lich.0001.c
		trigger = {
			martial >= 12
			is_available_adult_or_is_commanding = yes
		}
	}
}
﻿magic_counter_greater_or_equal = {
	third = HAS_MAGIC_COUNTER_VALUE
}

can_access_magic = {
	global = I_MUST_BE_A_MAGI
	global_not = I_AM_NOT_A_MAGI
	first = I_MUST_BE_A_MAGI
	first_not = I_AM_NOT_A_MAGI
}
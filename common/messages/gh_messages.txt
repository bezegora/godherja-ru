﻿gh_duel_toast_bad = {
	display = toast
	text = $DESC$
	desc = $LONG$
	tooltip = $TT$
	soundeffect = @toast_soundeffect_bad
	icon = "generic_bad_effect.dds"
	style = bad
}

gh_duel_toast_good = {
	display = toast
	text = $DESC$
	desc = $LONG$
	tooltip = $TT$
	soundeffect = @toast_soundeffect_good
	icon = "generic_good_effect.dds"
	style = good
}

gh_duel_feed_good = {
	display = feed
	text = $DESC$
	desc = $LONG$
	tooltip = $TT$
	soundeffect = @msg_good_soundeffect
	icon = "generic_good_effect.dds"
	style = good
}

gh_duel_feed_bad = {
	display = feed
	text = $DESC$
	desc = $LONG$
	tooltip = $TT$
	soundeffect = @msg_bad_soundeffect
	icon = "generic_bad_effect.dds"
	style = bad
}


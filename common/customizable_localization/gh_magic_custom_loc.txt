﻿GetMagicEducationLifestylePercentageBoost = {
	type = character
	text = {
		localization_key = education_rank_1_percentage
		trigger = {
			has_magic_education_rank_trigger = { RANK = 1 OPERATOR = equals }
		}
	}
	text = {
		localization_key = education_rank_2_percentage
		trigger = {
			has_magic_education_rank_trigger = { RANK = 2 OPERATOR = equals }
		}
	}

	text = {
		localization_key = education_rank_3_percentage
		trigger = {
			has_magic_education_rank_trigger = { RANK = 3 OPERATOR = equals }
		}
	}

	text = {
		localization_key = education_rank_4_percentage
		trigger = {
			has_magic_education_rank_trigger = { RANK = 4 OPERATOR = equals }
		}
	}
}

#GetMagicCounter = {
#	type = character
#	text = {
#		localization_key = "0"
#		trigger = {
#			this.var:magic_counter = { compare_value = 0 }
#		}
#	}
#	text = {
#		localization_key = "1"
#		trigger = {
#			this.var:magic_counter = { compare_value = 1 }
#		}
#	}
#	text = {
#		localization_key = "2"
#		trigger = {
#			this.var:magic_counter = { compare_value = 2 }
#		}
#	}
#	text = {
#		localization_key = "3"
#		trigger = {
#			this.var:magic_counter = { compare_value = 3 }
#		}
#	}
#	text = {
#		localization_key = "4"
#		trigger = {
#			this.var:magic_counter = { compare_value = 4 }
#		}
#	}
#	text = {
#		localization_key = "5"
#		trigger = {
#			this.var:magic_counter = { compare_value = 5 }
#		}
#	}
#	text = {
#		localization_key = "6"
#		trigger = {
#			this.var:magic_counter = { compare_value = 6 }
#		}
#	}
#	text = {
#		localization_key = "7"
#		trigger = {
#			this.var:magic_counter = { compare_value = 7 }
#		}
#	}
#	text = {
#		localization_key = "8"
#		trigger = {
#			this.var:magic_counter = { compare_value = 8 }
#		}
#	}
#	text = {
#		localization_key = "9"
#		trigger = {
#			this.var:magic_counter = { compare_value = 9 }
#		}
#	}
#	text = {
#		localization_key = "10"
#		trigger = {
#			this.var:magic_counter = { compare_value = 10 }
#		}
#	}
#	text = {
#		localization_key = "11"
#		trigger = {
#			this.var:magic_counter = { compare_value = 11 }
#		}
#	}
#	text = {
#		localization_key = "12"
#		trigger = {
#			this.var:magic_counter = { compare_value = 12 }
#		}
#	}
#	text = {
#		localization_key = "13"
#		trigger = {
#			this.var:magic_counter = { compare_value = 13 }
#		}
#	}
#	text = {
#		localization_key = "14"
#		trigger = {
#			this.var:magic_counter = { compare_value = 14 }
#		}
#	}
#	text = {
#		localization_key = "15"
#		trigger = {
#			this.var:magic_counter = { compare_value = 15 }
#		}
#	}
#	text = {
#		localization_key = "16"
#		trigger = {
#			this.var:magic_counter = { compare_value = 16 }
#		}
#	}
#	text = {
#		localization_key = "17"
#		trigger = {
#			this.var:magic_counter = { compare_value = 17 }
#		}
#	}
#	text = {
#		localization_key = "18"
#		trigger = {
#			this.var:magic_counter = { compare_value = 18 }
#		}
#	}
#	text = {
#		localization_key = "19"
#		trigger = {
#			this.var:magic_counter = { compare_value = 19 }
#		}
#	}
#	text = {
#		localization_key = "20"
#		trigger = {
#			this.var:magic_counter = { compare_value = 20 }
#		}
#	}
#	text = {
#		localization_key = "21"
#		trigger = {
#			this.var:magic_counter = { compare_value = 21 }
#		}
#	}
#	text = {
#		localization_key = "22"
#		trigger = {
#			this.var:magic_counter = { compare_value = 22 }
#		}
#	}
#	text = {
#		localization_key = "23"
#		trigger = {
#			this.var:magic_counter = { compare_value = 23 }
#		}
#	}
#	text = {
#		localization_key = "24"
#		trigger = {
#			this.var:magic_counter = { compare_value = 24 }
#		}
#	}
#	text = {
#		localization_key = "25"
#		trigger = {
#			this.var:magic_counter = { compare_value = 25 }
#		}
#	}
#	text = {
#		localization_key = "26"
#		trigger = {
#			this.var:magic_counter = { compare_value = 26 }
#		}
#	}
#	text = {
#		localization_key = "27"
#		trigger = {
#			this.var:magic_counter = { compare_value = 27 }
#		}
#	}
#	text = {
#		localization_key = "28"
#		trigger = {
#			this.var:magic_counter = { compare_value = 28 }
#		}
#	}
#	text = {
#		localization_key = "29"
#		trigger = {
#			this.var:magic_counter = { compare_value = 29 }
#		}
#	}
#	text = {
#		localization_key = "30"
#		trigger = {
#			this.var:magic_counter = { compare_value = 30 }
#		}
#	}
#}









# for gui scripting only; never actually shown

IsMagicCouncillorCustomLoc = {
	type = character
	text = {
		localization_key = "success"
		trigger = {
			has_council_position = councillor_court_mage
		}
	}

	text = {
		localization_key = "failure"
		fallback = yes
	}
}
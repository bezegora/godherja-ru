﻿gallieni_religion= {
	family = rf_pagan
	
	graphical_faith = "pagan_gfx"


	doctrine_background_icon = core_tenet_banner_pagan.dds
	doctrine = standard_hostility_doctrine
	#main group 
	doctrine = doctrine_gender_equal
	doctrine = doctrine_pluralism_righteous
	doctrine = doctrine_theocracy_lay_clergy
	doctrine = doctrine_no_head
	
	#Belief
	doctrine = doctrine_autonomous_tradition
	doctrine = doctrine_syncretic_mythology
	doctrine = doctrine_spiritual
	doctrine = doctrine_eternal
	doctrine = doctrine_spirits
	doctrine = doctrine_divine_guidance_none
	doctrine = doctrine_eschatology_none
	doctrine = doctrine_reincarnation

	#Practices
	doctrine = doctrine_holy_mortals_none
	doctrine = doctrine_autonomous
	doctrine = doctrine_no_restrictions
	doctrine = doctrine_monk_not_clergy_enforced
	doctrine = doctrine_atonement_none
	doctrine = doctrine_almsgiving_none
	
	#marriage
	doctrine = doctrine_monogamy
	doctrine = doctrine_consanguinity_cousins
	doctrine = doctrine_divorce_approval
	doctrine = doctrine_bastardry_legitimization
	doctrine = doctrine_sacred_marriage
	doctrine = doctrine_no_concubinage


	#crimes
	doctrine = doctrine_homosexuality_shunned
	doctrine = doctrine_adultery_men_shunned
	doctrine = doctrine_adultery_women_shunned
	doctrine = doctrine_kinslaying_extended_family_crime
	doctrine = doctrine_deviancy_crime
	doctrine = doctrine_magic_shunned
	doctrine = doctrine_slavery_neutral
	
	#Clerical Functions
	doctrine = doctrine_clerical_function_recruitment #replace with new called "teaching"
	doctrine = doctrine_clerical_gender_either
	doctrine = doctrine_clerical_marriage_allowed
	doctrine = doctrine_clerical_succession_temporal_appointment
	doctrine = doctrine_celebration_coronation
	
	doctrine = unreformed_faith_doctrine


	traits = {
		virtues = { lunatic_1 scholar }
		sins = { lazy cynical just  }
	}

	reserved_male_names = {
	}

	custom_faith_icons = {
		gallicader_custom_green_icon gallicader_custom_pink_icon gallicader_custom_purple_icon gallicader_custom_red_icon gallicader_custom_yellow_icon gallicader_icon 
	}

	holy_order_names = {
		{  }
	}

	holy_order_maa = {  }
		localization = {
			#HighGod
			HighGodName = gallicader_high_god_name
			HighGodNamePossessive = gallicader_high_god_name_possessive
			HighGodNameSheHe = CHARACTER_SHEHE_HE
			HighGodHerselfHimself = CHARACTER_HIMSELF
			HighGodHerHis = CHARACTER_HERHIS_HIS
			HighGodNameAlternate = gallicader_high_god_name_alternate
			HighGodNameAlternatePossessive = gallicader_high_god_name_alternate_possessive

			#Creator
			CreatorName = gallicader_creator_god_name
			CreatorNamePossessive = gallicader_creator_god_name_possessive
			CreatorSheHe = CHARACTER_SHEHE_HE
			CreatorHerHis = CHARACTER_HERHIS_HIS
			CreatorHerHim = CHARACTER_HERHIM_HIM

			#HealthGod
			HealthGodName = gallicader_health_god_name
			HealthGodNamePossessive = gallicader_health_god_name_possessive
			HealthGodSheHe = CHARACTER_SHEHE_HE
			HealthGodHerHis = CHARACTER_HERHIS_HIS
			HealthGodHerHim = CHARACTER_HERHIM_HIM
			
			#FertilityGod
			FertilityGodName = gallicader_fertility_god_name
			FertilityGodNamePossessive = gallicader_fertility_god_name_possessive
			FertilityGodSheHe = CHARACTER_SHEHE_HE
			FertilityGodHerHis = CHARACTER_HERHIS_HIS
			FertilityGodHerHim = CHARACTER_HERHIM_HIM

			#WealthGod
			WealthGodName = gallicader_wealth_god_name
			WealthGodNamePossessive = gallicader_wealth_god_name_possessive
			WealthGodSheHe = CHARACTER_SHEHE_HE
			WealthGodHerHis = CHARACTER_HERHIS_HIS
			WealthGodHerHim = CHARACTER_HERHIM_HIM

			#HouseholdGod
			HouseholdGodName = gallicader_household_god_name
			HouseholdGodNamePossessive = gallicader_household_god_name_possessive
			HouseholdGodSheHe = CHARACTER_SHEHE_HE
			HouseholdGodHerHis = CHARACTER_HERHIS_HIS
			HouseholdGodHerHim = CHARACTER_HERHIM_HIM

			#FateGod
			FateGodName = gallicader_fate_god_name
			FateGodNamePossessive = gallicader_fate_god_name_possessive
			FateGodSheHe = CHARACTER_SHEHE_HE
			FateGodHerHis = CHARACTER_HERHIS_HIS
			FateGodHerHim = CHARACTER_HERHIM_HIM

			#KnowledgeGod
			KnowledgeGodName = gallicader_knowledge_god_name
			KnowledgeGodNamePossessive = gallicader_knowledge_god_name_possessive
			KnowledgeGodSheHe = CHARACTER_SHEHE_HE
			KnowledgeGodHerHis = CHARACTER_HERHIS_HIS
			KnowledgeGodHerHim = CHARACTER_HERHIM_HIM

			#WarGod
			WarGodName = gallicader_war_god_name
			WarGodNamePossessive = gallicader_war_god_name_possessive
			WarGodSheHe = CHARACTER_SHEHE_HE
			WarGodHerHis = CHARACTER_HERHIS_HIS
			WarGodHerHim = CHARACTER_HERHIM_HIM

			#TricksterGod
			TricksterGodName = gallicader_trickster_god_name
			TricksterGodNamePossessive = gallicader_trickster_god_name_possessive
			TricksterGodSheHe = CHARACTER_SHEHE_HE
			TricksterGodHerHis = CHARACTER_HERHIS_HIS
			TricksterGodHerHim = CHARACTER_HERHIM_HIM

			#NightGod
			NightGodName = gallicader_night_god_name
			NightGodNamePossessive = gallicader_night_god_name_possessive
			NightGodSheHe = CHARACTER_SHEHE_HE
			NightGodHerHis = CHARACTER_HERHIS_HIS
			NightGodHerHim = CHARACTER_HERHIM_HIM

			#WaterGod
			WaterGodName = gallicader_water_god_name
			WaterGodNamePossessive = gallicader_water_god_name_possessive
			WaterGodSheHe = CHARACTER_SHEHE_HE
			WaterGodHerHis = CHARACTER_HERHIS_HIS
			WaterGodHerHim = CHARACTER_HERHIM_HIM



			PantheonTerm = gallicader_high_god_name
			PantheonTermHasHave = pantheon_term_has
			DevilName = gallicader_devil_name
			DevilNamePossessive = gallicader_devil_name_possessive
			DevilSheHe = CHARACTER_SHEHE_HE
			DevilHerHis = CHARACTER_HERHIS_HIS
			DevilHerselfHimself = CHARACTER_HIMSELF
			HouseOfWorship = gallicader_house_of_worship
			HouseOfWorshipPlural = gallicader_house_of_worship_plural
			ReligiousSymbol = gallicader_religious_symbol
			ReligiousText = gallicader_religious_text
			ReligiousHeadName = gallicader_religious_head_title
			ReligiousHeadTitleName = gallicader_religious_head_title_name
			DevoteeMale = gallicader_devotee_male
			DevoteeMalePlural = gallicader_devotee_male_plural
			DevoteeFemale = gallicader_devotee_female
			DevoteeFemalePlural = gallicader_devotee_female_plural
			DevoteeNeuter = gallicader_devotee_neuter
			DevoteeNeuterPlural = gallicader_devotee_neuter_plural
			PriestMale = gallicader_priest_male
			PriestMalePlural = gallicader_priest_male_plural
			PriestFemale = gallicader_priest_female
			PriestFemalePlural = gallicader_priest_female_plural
			PriestNeuter = gallicader_priest_male
			PriestNeuterPlural = gallicader_priest_male_plural
			AltPriestTermPlural = gallicader_priest_alternate_plural
			BishopMale = gallicader_bishop
			BishopMalePlural = gallicader_bishop_plural
			BishopFemale = gallicader_bishop
			BishopFemalePlural = gallicader_bishop_plural
			BishopNeuter = gallicader_bishop
			BishopNeuterPlural = gallicader_bishop_plural
			DivineRealm = gallicader_positive_afterlife
			PositiveAfterLife = gallicader_positive_afterlife
			NegativeAfterLife = gallicader_negative_afterlife
			DeathDeityName = gallicader_death_deity_name
			DeathDeityNamePossessive = gallicader_death_deity_name_possessive
			DeathDeitySheHe = CHARACTER_SHEHE_HE
			DeathDeityHerHis = CHARACTER_HERHIS_HIS
			WitchGodName = christianity_witchgodname_iblis
			WitchGodHerHis = CHARACTER_HERHIS_HIS
			WitchGodSheHe = CHARACTER_SHEHE_HE
			WitchGodHerHim = CHARACTER_HERHIM_HIM
			WitchGodMistressMaster = master
			WitchGodMotherFather = father


			GHWName = ghw_great_holy_war
			GHWNamePlural = ghw_great_holy_war
			
			GoodGodNames = {
			gallicader_high_god_name
			gallicader_fertility_god_name
			}
			EvilGodNames = {
			gallicader_witchgodname
			gallicader_devil_name
			gallicader_death_deity_name
			}
	}


	faiths = {
		gallicader = { #native religion of the marches
			color = { 1.00 0.81 0.80 }
			icon = gallicader_icon
			religious_head = d_gallicader_rel
			reformed_icon = gallicader_icon
			doctrine = unreformed_faith_doctrine

			#Tenet:
			doctrine = tenet_mystical_birthright
			doctrine = tenet_false_conversion_sanction 
			doctrine = tenet_astrology

			holy_site = andalau
			holy_site = connac
			holy_site = guizforte
			holy_site = arubosc
			holy_site = pekwarde
			holy_site = utepode
			
			#Crimes
			doctrine = doctrine_divorce_allowed
			doctrine = doctrine_bastardry_all
			doctrine = doctrine_homosexuality_accepted
			doctrine = doctrine_deviancy_accepted
			doctrine = doctrine_adultery_men_accepted
			doctrine = doctrine_adultery_women_accepted
			doctrine = doctrine_kinslaying_accepted
			
			doctrine = doctrine_consanguinity_unrestricted
			doctrine = doctrine_pluralism_pluralistic
			doctrine = doctrine_magic_crime

		}
	
	}
}
		
﻿marcher_religion= {
	family = gh_marcher
	
	graphical_faith = "catholic_gfx"


	doctrine_background_icon = core_tenet_banner_christian.dds
	doctrine = standard_hostility_doctrine
	doctrine = marcher_faith_doctrine

	#main group 
	doctrine = doctrine_gender_equal
	doctrine = doctrine_pluralism_righteous
	doctrine = doctrine_theocracy_lay_clergy
	doctrine = doctrine_no_head
	
	#Belief
	doctrine = doctrine_sacred_scriptures
	doctrine = doctrine_agnostic
	doctrine = doctrine_personal
	doctrine = doctrine_eternal
	doctrine = doctrine_deities_none
	doctrine = doctrine_divine_guidance_none
	doctrine = doctrine_eschatology_none
	doctrine = doctrine_underworld

	#Practices
	doctrine = doctrine_saints
	doctrine = doctrine_autonomous
	doctrine = doctrine_curtailed
	doctrine = doctrine_monk_not_clergy_enforced
	doctrine = doctrine_mandatory_atone
	doctrine = doctrine_encouraged_alms
	
	#marriage
	doctrine = doctrine_monogamy
	doctrine = doctrine_consanguinity_cousins
	doctrine = doctrine_divorce_approval
	doctrine = doctrine_bastardry_legitimization
	doctrine = doctrine_worldly_marriage
	doctrine = doctrine_paramourial

	#crimes
	doctrine = doctrine_homosexuality_shunned
	doctrine = doctrine_adultery_men_shunned
	doctrine = doctrine_adultery_women_shunned
	doctrine = doctrine_kinslaying_extended_family_crime
	doctrine = doctrine_deviancy_crime
	doctrine = doctrine_magic_shunned
	doctrine = doctrine_slavery_banned
	

	
	#Clerical Functions
	doctrine = doctrine_clerical_function_recruitment #replace with new called "teaching"
	doctrine = doctrine_clerical_gender_either
	doctrine = doctrine_clerical_marriage_allowed
	doctrine = doctrine_clerical_succession_temporal_appointment
	doctrine = doctrine_religious_coronation

    #Special Doctrine for Aversarian Syncretism
    doctrine = special_doctrine_is_marcher_faith
    
	


	traits = {
		virtues = { just }
		sins = { arbitrary }
	}

	reserved_male_names = {
	}

	custom_faith_icons = {
		waypath_custom_orange_icon waypath_custom_purple_icon waypath_custom_red_icon waypath_custom_teal_icon waypath_custom_yellow_icon waypath_custom_green_icon waypath_icon 
	}

	holy_order_names = {
		{  }
	}

	holy_order_maa = { marcher_anti_mage }

	localization = {
		#HighGod
		HighGodName = marcher_high_god_name
		HighGodNamePossessive = marcher_high_god_name_possessive
		HighGodNameSheHe = CHARACTER_SHEHE_HE
		HighGodHerselfHimself = CHARACTER_HIMSELF
		HighGodHerHis = CHARACTER_HERHIS_HIS
		HighGodNameAlternate = marcher_high_god_name_alternate
		HighGodNameAlternatePossessive = marcher_high_god_name_alternate_possessive

		#Creator
		CreatorName = marcher_creator_god_name
		CreatorNamePossessive = marcher_creator_god_name_possessive
		CreatorSheHe = CHARACTER_SHEHE_HE
		CreatorHerHis = CHARACTER_HERHIS_HIS
		CreatorHerHim = CHARACTER_HERHIM_HIM

		#HealthGod
		HealthGodName = marcher_health_god_name
		HealthGodNamePossessive = marcher_health_god_name_possessive
		HealthGodSheHe = CHARACTER_SHEHE_HE
		HealthGodHerHis = CHARACTER_HERHIS_HIS
		HealthGodHerHim = CHARACTER_HERHIM_HIM
		
		#FertilityGod
		FertilityGodName = marcher_fertility_god_name
		FertilityGodNamePossessive = marcher_fertility_god_name_possessive
		FertilityGodSheHe = CHARACTER_SHEHE_SHE
		FertilityGodHerHis = CHARACTER_HERHIS_HER
		FertilityGodHerHim = CHARACTER_HERHIM_HER

		#WealthGod
		WealthGodName = marcher_wealth_god_name
		WealthGodNamePossessive = marcher_wealth_god_name_possessive
		WealthGodSheHe = CHARACTER_SHEHE_HE
		WealthGodHerHis = CHARACTER_HERHIS_HIS
		WealthGodHerHim = CHARACTER_HERHIM_HIM

		#HouseholdGod
		HouseholdGodName = marcher_household_god_name
		HouseholdGodNamePossessive = marcher_household_god_name_possessive
		HouseholdGodSheHe = CHARACTER_SHEHE_HE
		HouseholdGodHerHis = CHARACTER_HERHIS_HIS
		HouseholdGodHerHim = CHARACTER_HERHIM_HIM

		#FateGod
		FateGodName = marcher_fate_god_name
		FateGodNamePossessive = marcher_fate_god_name_possessive
		FateGodSheHe = CHARACTER_SHEHE_SHE
		FateGodHerHis = CHARACTER_HERHIS_HER
		FateGodHerHim = CHARACTER_HERHIM_HER

		#KnowledgeGod
		KnowledgeGodName = marcher_knowledge_god_name
		KnowledgeGodNamePossessive = marcher_knowledge_god_name_possessive
		KnowledgeGodSheHe = CHARACTER_SHEHE_HE
		KnowledgeGodHerHis = CHARACTER_HERHIS_HIS
		KnowledgeGodHerHim = CHARACTER_HERHIM_HIM

		#WarGod
		WarGodName = marcher_war_god_name
		WarGodNamePossessive = marcher_war_god_name_possessive
		WarGodSheHe = CHARACTER_SHEHE_HE
		WarGodHerHis = CHARACTER_HERHIS_HIS
		WarGodHerHim = CHARACTER_HERHIM_HIM

		#TricksterGod
		TricksterGodName = marcher_trickster_god_name
		TricksterGodNamePossessive = marcher_trickster_god_name_possessive
		TricksterGodSheHe = CHARACTER_SHEHE_HE
		TricksterGodHerHis = CHARACTER_HERHIS_HIS
		TricksterGodHerHim = CHARACTER_HERHIM_HIM

		#NightGod
		NightGodName = marcher_night_god_name
		NightGodNamePossessive = marcher_night_god_name_possessive
		NightGodSheHe = CHARACTER_SHEHE_HE
		NightGodHerHis = CHARACTER_HERHIS_HIS
		NightGodHerHim = CHARACTER_HERHIM_HIM

		#WaterGod
		WaterGodName = marcher_water_god_name
		WaterGodNamePossessive = marcher_water_god_name_possessive
		WaterGodSheHe = CHARACTER_SHEHE_SHE
		WaterGodHerHis = CHARACTER_HERHIS_HER
		WaterGodHerHim = CHARACTER_HERHIM_HER



		PantheonTerm = marcher_high_god_name
		PantheonTermHasHave = pantheon_term_has
		GoodGodNames = {
			marcher_high_god_name
			HighGodNameAlternate
			marcher_health_god_name
			marcher_creator_god_name
			marcher_fertility_god_name
			marcher_household_god_name
			marcher_knowledge_god_name
			marcher_war_god_name
			marcher_trickster_god_name
		}
		DevilName = marcher_devil_name
		DevilNamePossessive = marcher_devil_name_possessive
		DevilSheHe = CHARACTER_SHEHE_HE
		DevilHerHis = CHARACTER_HERHIS_HIS
		DevilHerselfHimself = CHARACTER_HIMSELF
		EvilGodNames = {
			marcher_devil_name
			marcher_night_god_name
			marcher_fate_god_name
		}
		HouseOfWorship = marcher_house_of_worship
		HouseOfWorshipPlural = marcher_house_of_worship_plural
		ReligiousSymbol = marcher_religious_symbol
		ReligiousText = marcher_religious_text
		ReligiousHeadName = marcher_religious_head_title
		ReligiousHeadTitleName = marcher_religious_head_title_name
		DevoteeMale = marcher_devotee
		DevoteeMalePlural = marcher_devotee_plural
		DevoteeFemale = marcher_devotee
		DevoteeFemalePlural = marcher_devotee_plural
		DevoteeNeuter = marcher_devotee
		DevoteeNeuterPlural = marcher_devotee_plural
		PriestMale = marcher_priest
		PriestMalePlural = marcher_priest_plural
		PriestFemale = marcher_priest
		PriestFemalePlural = marcher_priest_plural
		PriestNeuter = marcher_priest
		PriestNeuterPlural = marcher_priest_plural
		AltPriestTermPlural = marcher_priest_alternate_plural
		BishopMale = marcher_bishop
		BishopMalePlural = marcher_bishop_plural
		BishopFemale = marcher_bishop
		BishopFemalePlural = marcher_bishop_plural
		BishopNeuter = marcher_bishop
		BishopNeuterPlural = marcher_bishop_plural
		DivineRealm = marcher_positive_afterlife
		PositiveAfterLife = marcher_positive_afterlife
		NegativeAfterLife = marcher_negative_afterlife
		DeathDeityName = marcher_death_deity_name
		DeathDeityNamePossessive = marcher_death_deity_name_possessive
		DeathDeitySheHe = CHARACTER_SHEHE_HE
		DeathDeityHerHis = CHARACTER_HERHIS_HIS
		WitchGodName = christianity_witchgodname_iblis
		WitchGodHerHis = CHARACTER_HERHIS_HIS
		WitchGodSheHe = CHARACTER_SHEHE_SHE
		WitchGodHerHim = CHARACTER_HERHIM_HER
		WitchGodMistressMaster = mistress
		WitchGodMotherFather = mother


		GHWName = ghw_great_holy_war
		GHWNamePlural = ghw_great_holy_war
	}


	faiths = {
		lifepath = { #marcher Religion
			color = { 0.6 0.5 0.1 }
			icon = waypath_icon
			religious_head = d_marcher_rel

			holy_site = peti_gisredde
			holy_site = the_crucible
			holy_site = arubosc
			holy_site = geedern

			#Tenet:
			doctrine = tenet_communal_identity
			doctrine = tenet_wayfathers 
			doctrine = tenet_monasticism

		}
		becledienism = { #Becledienism, founded by the radical monk Urso Becledièvre, rejects the concept of choosing one's lifepath and claims that a lifepath is something foretold at birth - considered extremely heretical despite a brief spell of popularity in the east. one of four religions from "The War of the Heretic Princes"
			color = { 0.5 0.6 0.1 }
			icon = waypath_custom_orange_icon
			religious_head = d_marcher_bec_rel

			holy_site = peti_gisredde
			holy_site = the_crucible
			holy_site = arubosc
			holy_site = geedern

			#Tenet:
			doctrine = tenet_astrology
			doctrine = tenet_wayfathers 
			doctrine = tenet_sacred_childbirth

		}
		darrallianism = { #Darralianism, founded by radical monk Cathrène den va Darrall, had some very esoteric concepts about Marcher religion and preached that the only true paths were the path of peace, the path of war, and the path of death, and also wanted all priests to live entirely within covens along with their flock in a psuedo-theocracy. Also stipulated that anyone could become a priest, which led to several lords naming themselves priests and claiming that the Marches were thus rightfully in their coven. one of four religions from "The War of the Heretic Princes"
			color = { 0.6 0.3 0.1 }
			icon = waypath_custom_purple_icon
			religious_head = d_marcher_dar_rel

			holy_site = peti_gisredde
			holy_site = the_crucible
			holy_site = arubosc
			holy_site = geedern

			#Tenet:
			doctrine = tenet_communal_identity
			doctrine = tenet_wayfathers 
			doctrine = tenet_monasticism

		}
		arderism = { #Arderists (from the Marcher word for burning) preached that the idolization of gods, wayfathers and of anything from the world not of the material was a grave sin and believed that by sealing onesself off from the 'ethereal' that the liches (and all magi) would be destroyed. Adherents looted and burned many churches and holdings, and several lords used the religion to landgrab hard from the church - indirectly leading to the overall weakness of the 'central' church in marcher society. nobody knows who founded it, its founder, the "Black Monk" was never actually found or killed. one of four religions from "The War of the Heretic Princes"
			color = { 0.7 0.5 0.1 }
			icon = waypath_custom_yellow_icon
			religious_head = d_marcher_ard_rel

			holy_site = peti_gisredde
			holy_site = the_crucible
			holy_site = arubosc
			holy_site = geedern

			#Tenet:
			doctrine = tenet_aniconism
			doctrine = tenet_wayfathers 
			doctrine = tenet_mendicant_preachers

		}
		saintsmarch = { #'Saintsmarch' could be described as a revanchist psuedo-religion/philosophy that was spearheaded by the radical prophet and anti-magi wildermutt den va seysoinns. a highly militarized version of the lifepath, adherents placed the war of nullification and the way of swords above all, and claimed that the marches could not live in peace until every magi was dead. Briefly entertained by several lords, but rapidly spun out of control once adherents started sacking foreign quarters and attacking the chancellors of local lords, and then started lynching lords themselves who they saw as not being spartan enough for the conflict. wildermutt was eventually turned on by his own followers once it was revealed he had been a way of harvests follower who had faked his path, and the religion soon collapsed. one of the four religions from "The War of the Heretic Princes"
			color = { 0.3 0.25 0.05 }
			icon = waypath_custom_red_icon
			religious_head = d_marcher_saints_rel

			holy_site = peti_gisredde
			holy_site = the_crucible
			holy_site = arubosc
			holy_site = geedern

			#Tenet:
			doctrine = tenet_unrelenting_faith
			doctrine = tenet_wayfathers 
			doctrine = tenet_warmonger

		}
		innerpath = { #insular lifepath (similar to insular christianity) that has a long history of popularity in the interior. was used to convert the gallicaders, but has slowly fallen out of favor and been shunned out as time has passed. still popular in some circles.
			color = { 0.7 0.5 0.3 }
			icon = waypath_custom_green_icon
			religious_head = d_marcher_inner_rel

			holy_site = peti_gisredde
			holy_site = the_crucible
			holy_site = arubosc
			holy_site = geedern

			#Tenet:
			doctrine = tenet_pastoral_isolation
			doctrine = tenet_wayfathers 
			doctrine = tenet_communal_identity

		}
		coastpath = { #nickname for a variety of folk religions from konijkmener and around the southeast coast that effectively worshipped their native oceanic pantheons with a lifepath coat. Was eventually stomped out by the konijkmener kings and fell rapidly out of favor in the Marches soon after.
			color = { 0.9 0.4 0.1 }
			icon = waypath_custom_teal_icon
			religious_head = d_marcher_coast_rel

			holy_site = peti_gisredde
			holy_site = the_crucible
			holy_site = arubosc
			holy_site = geedern

			#Tenet:
			doctrine = tenet_pastoral_isolation
			doctrine = tenet_wayfathers 
			doctrine = tenet_sanctity_of_nature

		}
		four_steps_of_salvation = { #what happens when marchers have satanism, batshit evil, formed by Thiscul den va Vescot and led to him and his entire family getting erased
			color = { 0.2 0.05 0.0 }
			icon = four_steps_of_salvation_icon
			religious_head = d_marcher_batshit_rel

			holy_site = peti_gisredde
			holy_site = the_crucible
			holy_site = arubosc
			holy_site = geedern

			#Tenet:
			doctrine = tenet_pursuit_of_power
			doctrine = tenet_ritual_cannibalism 
			doctrine = tenet_exaltation_of_pain

		}
	}
}
		
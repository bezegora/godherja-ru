﻿quwazaw_religion = {
	family = gh_sarradonian
	doctrine = standard_hostility_doctrine
	doctrine = doctrine_polygamy
	doctrine = doctrine_gender_equal
	doctrine = doctrine_consanguinity_aunt_nephew_and_uncle_niece
	doctrine = doctrine_divorce_allowed
	doctrine = doctrine_bastardry_none
	doctrine = doctrine_homosexuality_shunned

	doctrine = doctrine_adultery_men_shunned
	doctrine = doctrine_adultery_women_shunned
	doctrine = doctrine_kinslaying_shunned

	doctrine = doctrine_deviancy_crime
	
	doctrine = doctrine_magic_accepted

	doctrine = doctrine_pluralism_righteous
	doctrine = doctrine_theocracy_lay_clergy
	
	#Belief
	doctrine = doctrine_sacred_scriptures
	doctrine = doctrine_polytheistic
	doctrine = doctrine_aeonic
	doctrine = doctrine_cyclic
	doctrine = doctrine_spirits
	doctrine = doctrine_prophets
	doctrine = doctrine_judgement_day
	doctrine = doctrine_paradise_and_underworld
	doctrine = doctrine_worldly_marriage
	doctrine = doctrine_no_concubinage

	#Practices
	doctrine = doctrine_gurus_2
	doctrine = doctrine_autonomous
	doctrine = doctrine_encouraged
	doctrine = doctrine_monk_not_clergy_enforced
	doctrine = doctrine_encouraged_atone
	doctrine = doctrine_almsgiving_none
	
	doctrine = doctrine_slavery_encouraged
	
	doctrine = doctrine_clerical_function_recruitment
	doctrine = doctrine_clerical_gender_either
	doctrine = doctrine_clerical_marriage_allowed
	doctrine = doctrine_clerical_succession_temporal_fixed_appointment
	doctrine = doctrine_formal_coronation

	doctrine = doctrine_no_head

	traits = {
		virtues = { greedy compassionate diligent }
		sins = { generous sadistic lazy }
	}

	reserved_male_names = {
		Ammeris Apries Abar Ashakhet Amethu Baqet Bakenran Beketaten Benerib Buneb Dedi Djedefhor Djet Harsiese Harkhuf Horbaef Inhote Inaros Intef Itet Kashta Kahyu Khnum Menhet Menwi Meru Nebet Neferet Nuya
        Pensekhet Perneb Puimere Qareh Qakare Renseb Satien Senedj Sharek Tahip Tazka Zoser
	}

	custom_faith_icons = {
		quwazaw_custom_blue_icon quwazaw_custom_green_icon quwazaw_custom_red_icon quwazaw_icon
	}

	holy_order_names = {
		{ name = "holy_order_mekter_netaru" }
	}

	holy_order_maa = { }

	localization = {
		#HighGod
		HighGodName = quwazaw_high_god_name
		HighGodNamePossessive = quwazaw_high_god_name_possessive
		HighGodNameSheHe = CHARACTER_SHEHE_HE
		HighGodHerselfHimself = CHARACTER_HIMSELF
		HighGodHerHis = CHARACTER_HERHIS_HIS
		HighGodNameAlternate = quwazaw_high_god_name_alternate
		HighGodNameAlternatePossessive = quwazaw_high_god_name_alternate_possessive

		#Creator
		CreatorName = quwazaw_creator_god_name
		CreatorNamePossessive = quwazaw_creator_god_name_possessive
		CreatorSheHe = CHARACTER_SHEHE_HE
		CreatorHerHis = CHARACTER_HERHIS_HIS
		CreatorHerHim = CHARACTER_HERHIM_HIM

		#HealthGod
		HealthGodName = quwazaw_health_god_name
		HealthGodNamePossessive = quwazaw_health_god_name_possessive
		HealthGodSheHe = CHARACTER_SHEHE_HE
		HealthGodHerHis = CHARACTER_HERHIS_HIS
		HealthGodHerHim = CHARACTER_HERHIM_HIM
		
		#FertilityGod
		FertilityGodName = quwazaw_fertility_god_name
		FertilityGodNamePossessive = quwazaw_fertility_god_name_possessive
		FertilityGodSheHe = CHARACTER_SHEHE_HE
		FertilityGodHerHis = CHARACTER_HERHIS_HIS
		FertilityGodHerHim = CHARACTER_HERHIM_HIM

		#WealthGod
		WealthGodName = quwazaw_wealth_god_name
		WealthGodNamePossessive = quwazaw_wealth_god_name_possessive
		WealthGodSheHe = CHARACTER_SHEHE_HE
		WealthGodHerHis = CHARACTER_HERHIS_HIS
		WealthGodHerHim = CHARACTER_HERHIM_HIM

		#HouseholdGod
		HouseholdGodName = quwazaw_household_god_name
		HouseholdGodNamePossessive = quwazaw_household_god_name_possessive
		HouseholdGodSheHe = CHARACTER_SHEHE_HE
		HouseholdGodHerHis = CHARACTER_HERHIS_HIS
		HouseholdGodHerHim = CHARACTER_HERHIM_HIM

		#FateGod
		FateGodName = quwazaw_fate_god_name
		FateGodNamePossessive = quwazaw_fate_god_name_possessive
		FateGodSheHe = CHARACTER_SHEHE_HE
		FateGodHerHis = CHARACTER_HERHIS_HIS
		FateGodHerHim = CHARACTER_HERHIM_HIM

		#KnowledgeGod
		KnowledgeGodName = quwazaw_knowledge_god_name
		KnowledgeGodNamePossessive = quwazaw_knowledge_god_name_possessive
		KnowledgeGodSheHe = CHARACTER_SHEHE_HE
		KnowledgeGodHerHis = CHARACTER_HERHIS_HIS
		KnowledgeGodHerHim = CHARACTER_HERHIM_HIM

		#WarGod
		WarGodName = quwazaw_war_god_name
		WarGodNamePossessive = quwazaw_war_god_name_possessive
		WarGodSheHe = CHARACTER_SHEHE_HE
		WarGodHerHis = CHARACTER_HERHIS_HIS
		WarGodHerHim = CHARACTER_HERHIM_HIM

		#TricksterGod
		TricksterGodName = quwazaw_trickster_god_name
		TricksterGodNamePossessive = quwazaw_trickster_god_name_possessive
		TricksterGodSheHe = CHARACTER_SHEHE_HE
		TricksterGodHerHis = CHARACTER_HERHIS_HIS
		TricksterGodHerHim = CHARACTER_HERHIM_HIM

		#NightGod
		NightGodName = quwazaw_night_god_name
		NightGodNamePossessive = quwazaw_night_god_name_possessive
		NightGodSheHe = CHARACTER_SHEHE_HE
		NightGodHerHis = CHARACTER_HERHIS_HIS
		NightGodHerHim = CHARACTER_HERHIM_HIM

		#WaterGod
		WaterGodName = quwazaw_water_god_name
		WaterGodNamePossessive = quwazaw_water_god_name_possessive
		WaterGodSheHe = CHARACTER_SHEHE_HE
		WaterGodHerHis = CHARACTER_HERHIS_HIS
		WaterGodHerHim = CHARACTER_HERHIM_HIM



		PantheonTerm = quwazaw_high_god_name
		PantheonTermHasHave = pantheon_term_has
		GoodGodNames = {
			quwazaw_high_god_name
			HighGodNameAlternate
			quwazaw_health_god_name
			quwazaw_fertility_god_name
			quwazaw_household_god_name
			quwazaw_knowledge_god_name
			quwazaw_night_god_name
		}
		DevilName = quwazaw_devil_name
		DevilNamePossessive = quwazaw_devil_name_possessive
		DevilSheHe = CHARACTER_SHEHE_HE
		DevilHerHis = CHARACTER_HERHIS_HIS
		DevilHerselfHimself = CHARACTER_HIMSELF
		EvilGodNames = {
			quwazaw_devil_name
			quwazaw_wealth_god_name
			quwazaw_trickster_god_name
			quwazaw_witchgodname
		}
		HouseOfWorship = quwazaw_house_of_worship
		HouseOfWorshipPlural = quwazaw_house_of_worship_plural
		ReligiousSymbol = quwazaw_religious_symbol
		ReligiousText = quwazaw_religious_text
		ReligiousHeadName = quwazaw_religious_head_title
		ReligiousHeadTitleName = quwazaw_religious_head_title_name
		DevoteeMale = quwazaw_devotee
		DevoteeMalePlural = quwazaw_devotee_plural
		DevoteeFemale = quwazaw_devotee
		DevoteeFemalePlural = quwazaw_devotee_plural
		DevoteeNeuter = quwazaw_devotee
		DevoteeNeuterPlural = quwazaw_devotee_plural
		PriestMale = quwazaw_priest
		PriestMalePlural = quwazaw_priest_plural
		PriestFemale = quwazaw_priest
		PriestFemalePlural = quwazaw_priest_plural
		PriestNeuter = quwazaw_priest
		PriestNeuterPlural = quwazaw_priest_plural
		AltPriestTermPlural = quwazaw_priest_alternate_plural
		BishopMale = quwazaw_bishop
		BishopMalePlural = quwazaw_bishop_plural
		BishopFemale = quwazaw_bishop
		BishopFemalePlural = quwazaw_bishop_plural
		BishopNeuter = quwazaw_bishop
		BishopNeuterPlural = quwazaw_bishop_plural
		DivineRealm = quwazaw_positive_afterlife
		PositiveAfterLife = quwazaw_positive_afterlife
		NegativeAfterLife = quwazaw_negative_afterlife
		DeathDeityName = quwazaw_death_deity_name
		DeathDeityNamePossessive = quwazaw_death_deity_name_possessive
		DeathDeitySheHe = CHARACTER_SHEHE_HE
		DeathDeityHerHis = CHARACTER_HERHIS_HIS
		WitchGodName = christianity_witchgodname_iblis
		WitchGodHerHis = CHARACTER_HERHIS_HIS
		WitchGodSheHe = CHARACTER_SHEHE_HE
		WitchGodHerHim = CHARACTER_HERHIM_HIM
		WitchGodMistressMaster = master
		WitchGodMotherFather = father


		GHWName = ghw_jihad
		GHWNamePlural = ghw_jihads
	}
	
	piety_icon_group = "islam"
	faiths = {
	
		quwazaw = { #sarradonian religion
			color = { 0.8 0.8 0.6 }
			icon = quwazaw_icon
			religious_head = d_quwazaw_rel

			holy_site = sulamaniyah
			holy_site = fendikyah
			holy_site = ifralimane
			holy_site = gharstir
			holy_site = saidal

			#Tenet:
			doctrine = tenet_armed_pilgrimages
			doctrine = tenet_mendicant_preachers
			doctrine = tenet_pentarchy
		}
	}
}
﻿blackhand_religion = {
	family = rf_imperial

	doctrine_background_icon = core_tenet_banner_christian.dds
	doctrine = standard_hostility_doctrine
	#Main Group
	doctrine = doctrine_no_head
	doctrine = doctrine_gender_equal
	doctrine = doctrine_pluralism_fundamentalist
	doctrine = doctrine_theocracy_temporal
	
	#Belief
	doctrine = doctrine_oral_tradition
	doctrine = doctrine_atheistic
	doctrine = doctrine_aeonic
	doctrine = doctrine_cyclic
	doctrine = doctrine_deities_none
	doctrine = doctrine_anointed_deliverer
	doctrine = doctrine_promised_deliverer
	doctrine = doctrine_paradise

	#Practices
	doctrine = doctrine_gurus_2
	doctrine = doctrine_autonomous
	doctrine = doctrine_no_restrictions
	doctrine = doctrine_monk_not_clergy_enforced
	doctrine = doctrine_atonement_none
	doctrine = doctrine_almsgiving_none
	
	#Marriage
	doctrine = doctrine_monogamy
	doctrine = doctrine_divorce_approval
	doctrine = doctrine_bastardry_legitimization
	doctrine = doctrine_consanguinity_aunt_nephew_and_uncle_niece
	doctrine = doctrine_no_concubinage
	doctrine = doctrine_none_marriage

	#Crimes
	doctrine = doctrine_homosexuality_shunned
	doctrine = doctrine_adultery_men_shunned
	doctrine = doctrine_adultery_women_shunned
	doctrine = doctrine_kinslaying_extended_family_crime
	doctrine = doctrine_deviancy_crime
	
	doctrine = doctrine_magic_accepted

	#Clerical Functions
	doctrine = doctrine_clerical_function_taxation
	doctrine = doctrine_clerical_gender_either
	doctrine = doctrine_clerical_marriage_allowed
	doctrine = doctrine_clerical_succession_temporal_appointment
	doctrine = doctrine_formal_coronation

	traits = {
		virtues = { wrathful deceitful }
		sins = { calm honest }
	}

	reserved_male_names = {
		Andrew Antoninus Bartolomeus Benedict Christian Christopher Clement Constantine David Demetrius Eustace George Gregory
		Hans Isaac Joakim Jacob John Jordan Joseph Laurence Magnus Marcus Martin Matthew Michael Nicholas Patrick Paul Peter Philip
		Salvador Samuel Sebastian Simon Stephen Thomas Abraham Alexander Daniel Jesus Job Joshua Cyrus Luke Zachariah
	}
	reserved_female_names = {
		Anna Beatrice Benedicta Brigid Catherine Cecilia Christina Clementia Constance Dorothy Elisabeth Joan Judith Juliana Helen
		Magdalena Margaret Maria Marine Patricia Philippa Rachel Sarah Sophia Stephania
	}

	custom_faith_icons = {
		blackhand_custom_blue_icon blackhand_custom_green_icon blackhand_custom_orange_icon blackhand_custom_red_icon blackhand_icon
	}

	holy_order_names = {
		{ name = "The Inner Circle" }
		{ name = "The MindTaker's Broken" }
	}

	holy_order_maa = {  }

	localization = {
		#HighGod
		HighGodName = agionism_high_god_name
		HighGodNamePossessive = agionism_high_god_name_possessive
		HighGodNameSheHe = CHARACTER_SHEHE_HE
		HighGodHerselfHimself = CHARACTER_HIMSELF
		HighGodHerHis = CHARACTER_HERHIS_HIS
		HighGodNameAlternate = agionism_high_god_name_alternate
		HighGodNameAlternatePossessive = agionism_high_god_name_alternate_possessive

		#Creator
		CreatorName = agionism_creator_god_name
		CreatorNamePossessive = agionism_creator_god_name_possessive
		CreatorSheHe = CHARACTER_SHEHE_HE
		CreatorHerHis = CHARACTER_HERHIS_HIS
		CreatorHerHim = CHARACTER_HERHIM_HIM

		#HealthGod
		HealthGodName = agionism_health_god_name
		HealthGodNamePossessive = agionism_health_god_name_possessive
		HealthGodSheHe = CHARACTER_SHEHE_HE
		HealthGodHerHis = CHARACTER_HERHIS_HIS
		HealthGodHerHim = CHARACTER_HERHIM_HIM
		
		#FertilityGod
		FertilityGodName = agionism_fertility_god_name
		FertilityGodNamePossessive = agionism_fertility_god_name_possessive
		FertilityGodSheHe = CHARACTER_SHEHE_HE
		FertilityGodHerHis = CHARACTER_HERHIS_HIS
		FertilityGodHerHim = CHARACTER_HERHIM_HIM

		#WealthGod
		WealthGodName = agionism_wealth_god_name
		WealthGodNamePossessive = agionism_wealth_god_name_possessive
		WealthGodSheHe = CHARACTER_SHEHE_HE
		WealthGodHerHis = CHARACTER_HERHIS_HIS
		WealthGodHerHim = CHARACTER_HERHIM_HIM

		#HouseholdGod
		HouseholdGodName = agionism_household_god_name
		HouseholdGodNamePossessive = agionism_household_god_name_possessive
		HouseholdGodSheHe = CHARACTER_SHEHE_HE
		HouseholdGodHerHis = CHARACTER_HERHIS_HIS
		HouseholdGodHerHim = CHARACTER_HERHIM_HIM

		#FateGod
		FateGodName = agionism_fate_god_name
		FateGodNamePossessive = agionism_fate_god_name_possessive
		FateGodSheHe = CHARACTER_SHEHE_HE
		FateGodHerHis = CHARACTER_HERHIS_HIS
		FateGodHerHim = CHARACTER_HERHIM_HIM

		#KnowledgeGod
		KnowledgeGodName = agionism_knowledge_god_name
		KnowledgeGodNamePossessive = agionism_knowledge_god_name_possessive
		KnowledgeGodSheHe = CHARACTER_SHEHE_HE
		KnowledgeGodHerHis = CHARACTER_HERHIS_HIS
		KnowledgeGodHerHim = CHARACTER_HERHIM_HIM

		#WarGod
		WarGodName = agionism_war_god_name
		WarGodNamePossessive = agionism_war_god_name_possessive
		WarGodSheHe = CHARACTER_SHEHE_HE
		WarGodHerHis = CHARACTER_HERHIS_HIS
		WarGodHerHim = CHARACTER_HERHIM_HIM

		#TricksterGod
		TricksterGodName = agionism_trickster_god_name
		TricksterGodNamePossessive = agionism_trickster_god_name_possessive
		TricksterGodSheHe = CHARACTER_SHEHE_HE
		TricksterGodHerHis = CHARACTER_HERHIS_HIS
		TricksterGodHerHim = CHARACTER_HERHIM_HIM

		#NightGod
		NightGodName = agionism_night_god_name
		NightGodNamePossessive = agionism_night_god_name_possessive
		NightGodSheHe = CHARACTER_SHEHE_HE
		NightGodHerHis = CHARACTER_HERHIS_HIS
		NightGodHerHim = CHARACTER_HERHIM_HIM

		#WaterGod
		WaterGodName = agionism_water_god_name
		WaterGodNamePossessive = agionism_water_god_name_possessive
		WaterGodSheHe = CHARACTER_SHEHE_HE
		WaterGodHerHis = CHARACTER_HERHIS_HIS
		WaterGodHerHim = CHARACTER_HERHIM_HIM

		PantheonTerm = agionism_high_god_name
		PantheonTermHasHave = pantheon_term_has
		GoodGodNames = {
			agionism_high_god_name
			agionism_high_god_name_alternate
			agionism_creator_god_name
			agionism_health_god_name
			agionism_wealth_god_name
			agionism_fertility_god_name
			agionism_knowledge_god_name
			agionism_war_god_name
			agionism_trickster_god_name
			agionism_death_deity_name
			agionism_household_god_name
		}
		DevilName = agionism_devil_name
		DevilNamePossessive = agionism_devil_name_possessive
		DevilSheHe = CHARACTER_SHEHE_HE
		DevilHerHis = CHARACTER_HERHIS_HIS
		DevilHerHis = CHARACTER_HERHIS_HIS
		DevilHerselfHimself = CHARACTER_HIMSELF
		EvilGodNames = {
			agionism_devil_name
			agionism_witchgodname
		}
		HouseOfWorship = agionism_house_of_worship
		HouseOfWorshipPlural = agionism_house_of_worship_plural
		ReligiousSymbol = agionism_religious_symbol
		ReligiousText = agionism_religious_text
		ReligiousHeadName = agionism_religious_head_title
		ReligiousHeadTitleName = agionism_religious_head_title_name
		DevoteeMale = agionism_devotee_male
		DevoteeMalePlural = agionism_devotee_male_plural
		DevoteeFemale = agionism_devotee_female
		DevoteeFemalePlural = agionism_devotee_female_plural
		DevoteeNeuter = agionism_devotee_neuter
		DevoteeNeuterPlural = agionism_devotee_neuter_plural
		PriestMale = agionism_priest_male
		PriestMalePlural = agionism_priest_male_plural
		PriestFemale = agionism_priest_male
		PriestFemalePlural = agionism_priest_male_plural
		PriestNeuter = agionism_priest_male
		PriestNeuterPlural = agionism_priest_male_plural
		AltPriestTermPlural = agionism_priest_alternate_plural
		BishopMale = agionism_bishop
		BishopMalePlural = agionism_bishop_plural
		BishopFemale = agionism_bishop
		BishopFemalePlural = agionism_bishop_plural
		BishopNeuter = agionism_bishop
		BishopNeuterPlural = agionism_bishop_plural
		DivineRealm = agionism_positive_afterlife
		PositiveAfterLife = agionism_positive_afterlife
		NegativeAfterLife = agionism_negative_afterlife
		DeathDeityName = agionism_death_deity_name
		DeathDeityNamePossessive = agionism_death_deity_name_possessive
		DeathDeitySheHe = CHARACTER_SHEHE_HE
		DeathDeityHerHis = CHARACTER_HERHIS_HIS
		WitchGodName = agionism_witchgodname
		WitchGodHerHis = CHARACTER_HERHIS_HIS
		WitchGodSheHe = CHARACTER_SHEHE_HE
		WitchGodHerHim = CHARACTER_HERHIM_HIM
		WitchGodMistressMaster = master
		WitchGodMotherFather = father


		GHWName = ghw_crusade
		GHWNamePlural = ghw_crusades

	}
	
	piety_icon_group = "christian"

	faiths = {
        blackhand = {
			color = { 0.1 0.1 0.1 }
			graphical_faith = "catholic_gfx"
			icon = blackhand_icon
			religious_head = d_blackhand_rel
			
			#Special Tolerance
			doctrine = divine_destiny_doctrine

			#Tenets
			doctrine = tenet_aniconism
			doctrine = tenet_mendicant_preachers
			doctrine = tenet_pursuit_of_power

			doctrine = doctrine_gender_equal
			doctrine = doctrine_clerical_gender_either

			holy_site = oraispol
			holy_site = asiupoli
			holy_site = golgembria
			holy_site = vounill


		}
    }
}
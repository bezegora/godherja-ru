﻿cumani_religion = {
	family = rf_pagan
	graphical_faith = pagan_gfx

	pagan_roots = yes

	doctrine = pagan_hostility_doctrine
	doctrine = standard_hostility_doctrine
	#Main Group
	doctrine = doctrine_no_head
	doctrine = doctrine_gender_male_dominated
	doctrine = doctrine_pluralism_pluralistic
	doctrine = doctrine_theocracy_temporal
	
	#Belief
	doctrine = doctrine_oral_tradition
	doctrine = doctrine_ditheistic
	doctrine = doctrine_ethereal
	doctrine = doctrine_dualistic
	doctrine = doctrine_deities_none
	doctrine = doctrine_avatars
	doctrine = doctrine_apocalypse
	doctrine = doctrine_paradise_and_underworld

	#Practices
	doctrine = doctrine_heroes
	doctrine = doctrine_autonomous
	doctrine = doctrine_no_restrictions
	doctrine = doctrine_monk_not_clergy_enforced
	doctrine = doctrine_encouraged_atone
	doctrine = doctrine_almsgiving_none

	#Marriage
	doctrine = doctrine_concubines
	doctrine = doctrine_divorce_allowed
	doctrine = doctrine_bastardry_legitimization
	doctrine = doctrine_consanguinity_cousins
	doctrine = doctrine_paramourial
	doctrine = doctrine_sacred_marriage
	doctrine = doctrine_monogamy


	#Crimes
	doctrine = doctrine_homosexuality_crime
	doctrine = doctrine_adultery_men_crime
	doctrine = doctrine_adultery_women_crime
	doctrine = doctrine_kinslaying_close_kin_crime
	doctrine = doctrine_deviancy_crime
	doctrine = doctrine_slavery_encouraged
	
	doctrine = doctrine_magic_accepted

	#Clerical Functions
	doctrine = doctrine_clerical_function_recruitment
	doctrine = doctrine_clerical_gender_either
	doctrine = doctrine_clerical_marriage_allowed
	doctrine = doctrine_clerical_succession_temporal_appointment
	doctrine = doctrine_celebration_coronation

	traits = {
		virtues = { brave honest gregarious }
		sins = { craven deceitful shy }
	}

	custom_faith_icons = {
		cumani_custom_blue_icon cumani_custom_green_icon cumani_custom_purple_icon cumani_custom_red_icon
	}

	holy_order_names = {

	}

	holy_order_maa = { horse_archers }

	localization = {
		HighGodName = cumani_high_god_name
		HighGodNamePossessive = cumani_high_god_name_possessive
		HighGodNameSheHe = CHARACTER_SHEHE_HE
		HighGodHerselfHimself = CHARACTER_HIMSELF
		HighGodHerHis = CHARACTER_HERHIS_HIS
		HighGodNameAlternate = tengrism_high_god_name_alternate
		HighGodNameAlternatePossessive = tengrism_high_god_name_alternate_possessive

		#Creator
		CreatorName = cumani_creator_god_name
		CreatorNamePossessive = cumani_creator_god_name_possessive
		CreatorSheHe = CHARACTER_SHEHE_HE
		CreatorHerHis = CHARACTER_HERHIS_HIS
		CreatorHerHim = CHARACTER_HERHIM_HIM

		#HealthGod
		HealthGodName = cumani_health_god_name
		HealthGodNamePossessive = cumani_health_god_name_possessive
		HealthGodSheHe = CHARACTER_SHEHE_HE
		HealthGodHerHis = CHARACTER_HERHIS_HIS
		HealthGodHerHim = CHARACTER_HERHIM_HIM
		
		#FertilityGod
		FertilityGodName = cumani_fertility_god_name
		FertilityGodNamePossessive = cumani_fertility_god_name_possessive
		FertilityGodSheHe = CHARACTER_SHEHE_HE
		FertilityGodHerHis = CHARACTER_HERHIS_HIS
		FertilityGodHerHim = CHARACTER_HERHIM_HIM

		#WealthGod
		WealthGodName = cumani_wealth_god_name
		WealthGodNamePossessive = cumani_wealth_god_name_possessive
		WealthGodSheHe = CHARACTER_SHEHE_HE
		WealthGodHerHis = CHARACTER_HERHIS_HIS
		WealthGodHerHim = CHARACTER_HERHIM_HIM

		#HouseholdGod
		HouseholdGodName = cumani_household_god_name
		HouseholdGodNamePossessive = cumani_household_god_name_possessive
		HouseholdGodSheHe = CHARACTER_SHEHE_HE
		HouseholdGodHerHis = CHARACTER_HERHIS_HIS
		HouseholdGodHerHim = CHARACTER_HERHIM_HIM

		#FateGod
		FateGodName = cumani_fate_god_name
		FateGodNamePossessive = cumani_fate_god_name_possessive
		FateGodSheHe = CHARACTER_SHEHE_IT
		FateGodHerHis = CHARACTER_HERHIS_ITS
		FateGodHerHim = CHARACTER_HERHIM_IT

		#KnowledgeGod
		KnowledgeGodName = cumani_knowledge_god_name
		KnowledgeGodNamePossessive = cumani_knowledge_god_name_possessive
		KnowledgeGodSheHe = CHARACTER_SHEHE_HE
		KnowledgeGodHerHis = CHARACTER_HERHIS_HIS
		KnowledgeGodHerHim = CHARACTER_HERHIM_HIM

		#WarGod
		WarGodName = cumani_war_god_name
		WarGodNamePossessive = cumani_war_god_name_possessive
		WarGodSheHe = CHARACTER_SHEHE_HE
		WarGodHerHis = CHARACTER_HERHIS_HIS
		WarGodHerHim = CHARACTER_HERHIM_HIM

		#TricksterGod
		TricksterGodName = cumani_trickster_god_name
		TricksterGodNamePossessive = cumani_trickster_god_name_possessive
		TricksterGodSheHe = CHARACTER_SHEHE_SHE
		TricksterGodHerHis = CHARACTER_HERHIS_HER
		TricksterGodHerHim = CHARACTER_HERHIM_HER

		#NightGod
		NightGodName = cumani_night_god_name
		NightGodNamePossessive = cumani_night_god_name_possessive
		NightGodSheHe = CHARACTER_SHEHE_HE
		NightGodHerHis = CHARACTER_HERHIS_HIS
		NightGodHerHim = CHARACTER_HERHIM_HIM

		#WaterGod
		WaterGodName = cumani_water_god_name
		WaterGodNamePossessive = cumani_water_god_name_possessive
		WaterGodSheHe = CHARACTER_SHEHE_HE
		WaterGodHerHis = CHARACTER_HERHIS_HIS
		WaterGodHerHim = CHARACTER_HERHIM_HIM



		PantheonTerm = religion_the_gods
		PantheonTermHasHave = pantheon_term_have
		GoodGodNames = { 
			cumani_war_god_name
			cumani_creator_god_name 
			cumani_health_god_name 
			cumani_fertility_god_name 
			cumani_water_god_name
		}
		DevilName = cumani_devil_name
		DevilNamePossessive = cumani_devil_name_possessive
		DevilSheHe = CHARACTER_SHEHE_HE
		DevilHerHis = CHARACTER_HERHIS_HIS
		DevilHerselfHimself = CHARACTER_HIMSELF
		EvilGodNames = { 
			cumani_trickster_god_name
			cumani_night_god_name
			cumani_fate_god_name
		}
		HouseOfWorship = cumani_house_of_worship
		HouseOfWorshipPlural = cumani_religious_text
		ReligiousSymbol = cumani_religious_symbol
		ReligiousText = cumani_religious_text
		ReligiousHeadName = cumani_religious_head_title
		ReligiousHeadTitleName = cumani_religious_head_title_name
		DevoteeMale = cumani_devotee_male
		DevoteeMalePlural = cumani_devotee_male_plural
		DevoteeFemale = cumani_devotee_female
		DevoteeFemalePlural = cumani_devotee_female_plural
		DevoteeNeuter = cumani_devotee_neuter
		DevoteeNeuterPlural = cumani_devotee_neuter_plural
		PriestMale = cumani_priest_male
		PriestMalePlural = cumani_priest_male_plural
		PriestFemale = cumani_priest_female
		PriestFemalePlural = cumani_priest_female_plural
		PriestNeuter = cumani_priest_male
		PriestNeuterPlural = cumani_priest_male_plural
		AltPriestTermPlural = cumani_priest_alternate_plural
		BishopMale = cumani_bishop
		BishopMalePlural = cumani_bishop_plural
		BishopFemale = cumani_bishop
		BishopFemalePlural = cumani_bishop_plural
		BishopNeuter = cumani_bishop
		BishopNeuterPlural = cumani_bishop_plural
		DivineRealm = cumani_divine_realm
		PositiveAfterLife = cumani_divine_realm
		NegativeAfterLife = cumani_afterlife
		DeathDeityName = cumani_devil_name
		DeathDeityNamePossessive = cumani_devil_name_possessive
		DeathDeitySheHe = CHARACTER_SHEHE_HE
		DeathDeityHerHis = CHARACTER_HERSHIS_HIS
		WitchGodName = witchgod_cumani_szelanya
		WitchGodSheHe = CHARACTER_SHEHE_HE
		WitchGodHerHim = CHARACTER_HERHIM_HER
		WitchGodMistressMaster = mistress
		WitchGodMotherFather = mother

		WitchGodHerHis = CHARACTER_HERHIS_HER

		GHWName = ghw_great_holy_war
		GHWNamePlural = ghw_great_holy_wars
	}

	faiths = {
		cumani = {
			color = { 250 120 90 }
			icon = cumani_icon
			religious_head = d_cumani_rel
			reformed_icon = cumani_icon
			doctrine = unreformed_faith_doctrine

			holy_site = sexi
			holy_site = olbos
			holy_site = selous
			holy_site = ambracia
			holy_site = kosios
			holy_site = tulsos

			doctrine = tenet_the_soul_inside
			doctrine = tenet_astrology
			doctrine = tenet_ancestor_worship
		}
	}
}
﻿wayline_modifier = {
	icon = magic_positive
}

wayline_intersection_modifier = {
	icon = magic_positive
}

antimagic_field = {
	icon = magic_negative
}

arcane_fallout = {
	icon = magic_negative
}
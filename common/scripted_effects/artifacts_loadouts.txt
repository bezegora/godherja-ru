﻿# Here you can add a convenient set of loadouts that fit your mod, which can then be called in the character history files or on_game_start.
# Can also be added in the console by writing: 
# effect add_loadout_landsknecht = yes
# or simialr.



add_loadout_knight_1 = {
	add_artifact = {ARTIFACT = artifact_4}
	add_artifact = {ARTIFACT = artifact_28}
}
add_loadout_knight_2 = {
	add_artifact = {ARTIFACT = artifact_5}
	add_artifact = {ARTIFACT = artifact_29}
}
add_loadout_knight_3 = {
	add_artifact = {ARTIFACT = artifact_6}
	add_artifact = {ARTIFACT = artifact_30}
}
add_loadout_knight_3 = {
	add_artifact = {ARTIFACT = artifact_7}
	add_artifact = {ARTIFACT = artifact_31}
}

add_loadout_highprince = {
	add_artifact = {ARTIFACT = artifact_spanreed}
	add_artifact = {ARTIFACT = artifact_soulcaster}
}

add_loadout_shardbearer = {
	add_artifact = {ARTIFACT = artifact_shardblade}
	add_artifact = {ARTIFACT = artifact_shardplate}
}
pattern_texture_lists = {
	most_emblems_list = {
		25  = "pattern_checkers_01.dds"
		15 	= "pattern_checkers_diagonal_01.dds"
		#5  	= "pattern_checkers_diagonal_02.dds"
		25 	= "pattern_horizontal_split_01.dds"
		10 	= "pattern_horizontal_split_02.dds"
		15 	= "pattern_horizontal_stripes_01.dds"
		#100	= "pattern_solid.dds"
		25 	= "pattern_vertical_split_01.dds"
		5  	= "pattern_vertical_split_jagged_01.dds"
		10 	= "pattern_vertical_stripes_01.dds"
		15 	= "pattern_vertical_stripes_02.dds"
	}	
	
	basic_division = {
		100  = "pattern_solid.dds"
		20  = "pattern_vertical_split_01.dds"
		10  = "pattern_diagonal_split_01.dds"
		20  = "pattern_horizontal_split_01.dds"
		special_selection = {
			trigger = {
				OR = {
					scope:faith.religion = religion:aversarinas_aautokrata_religion
					scope:faith.religion = religion:agionism_religion
					scope:faith.religion = religion:marcher_religion
				}
			}
			5 	= "pattern_checkers_diagonal_01.dds"
			10  = "pattern_checkers_01.dds"	
			30  = "pattern_vertical_split_01.dds"			
		}			
	}
	
	mena_tierced_per_fess = {
		10  = "pattern_horizontal_bar_01.dds"
		1000  = "pattern_tricolor_horizontal_01.dds"
	}
	
	field_cross = {
		10  = "pattern_solid.dds"
		2  = "pattern_checkers_01.dds"
		1  = "pattern_horizontal_split_01.dds"
		1  = "pattern_vertical_split_01.dds"
	}
	field_saltire = {
		10  = "pattern_solid.dds"
		2  = "pattern_checkers_diagonal_01.dds"
		2  = "pattern_diagonal_split_01.dds"
		2  = "pattern_diagonal_split_02.dds"
	}

	###

	# Pagan - Simple & rare divisions of the field
	basic_pagan_patterns = {
		160  = "pattern_solid.dds"
		20  = "pattern_vertical_split_01.dds"
		10  = "pattern_diagonal_split_01.dds"
		20  = "pattern_horizontal_split_01.dds"
		special_selection = {
			trigger = {
				faith = {
					religion = { is_in_family = rf_pagan }
				}
			}
			40  = "pattern_diagonal_split_01.dds"
		}		
	}
	
	# No division of the field
	field_standalone = {
		100  = "pattern_solid.dds"
	}	
	
	# No division of the field
	faction_patterns_list = {
		20  = "pattern_vertical_split_01.dds"
		30  = "pattern_diagonal_split_01.dds"
		20  = "pattern_horizontal_split_01.dds"
		5  = "pattern_checkers_01.dds"
		5 	= "pattern_checkers_diagonal_01.dds"
	}	
}
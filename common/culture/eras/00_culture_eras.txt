﻿
culture_era_tribal = {
	year = 0
}

culture_era_early_medieval = {
	year = 1150
	invalid_for_government = tribal_government
}

culture_era_high_medieval = {
	year = 1325
	invalid_for_government = tribal_government
}

culture_era_late_medieval = {
	year = 1450
	invalid_for_government = tribal_government
}

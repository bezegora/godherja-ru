﻿gh_redlanders = {
	graphical_cultures = {
		mediterranean_building_gfx
		byzantine_clothing_gfx
		eastern_unit_gfx
	}
	mercenary_names = {
		{ name = "mercenary_company_legio_xv" }
		{ name = "mercenary_company_firestar_company" }
		{ name = "mercenary_company_emperors_wroth" }
		{ name = "mercenary_company_legio_ix" }
		{ name = "mercenary_company_legio_vii" }
		{ name = "mercenary_company_lost_sons" }
		{ name = "mercenary_company_hammer_company" }
		{ name = "mercenary_company_bearskin_company" }
		{ name = "mercenary_company_redstone_bandits" }
		{ name = "mercenary_company_legio_xxiv" }
		{ name = "mercenary_company_bloodspiller_company" }
		{ name = "mercenary_company_sons_of_destiny" }
		{ name = "mercenary_company_broken_arrows" }
		{ name = "mercenary_company_kingslayers" }
		{ name = "mercenary_company_old_reliables" }
		{ name = "mercenary_company_greyskull_company" }
		{ name = "mercenary_company_snakesorrow_bandits" }
		{ name = "mercenary_company_gravedigers" }
		{ name = "mercenary_company_axiaotheas_revenge" }
		{ name = "mercenary_company_crowntakers" }
		{ name = "mercenary_company_golden_nose_band" }
		{ name = "mercenary_company_champions_of_the_rock" }
		{ name = "mercenary_company_rainfall_company" }
		{ name = "mercenary_company_endless_survivors" }
	}

	redlander = { #Creepy cultist folk

		graphical_cultures = {
			northern_clothing_gfx
		}
		
		color = { 0.0 0.0 0.0 }
		
		cadet_dynasty_names = {
			"dynn_Adephmos"
			"dynn_Aeteos"
			"dynn_Avalanthangelos"
			"dynn_Andinenvo"
			"dynn_Avariaknakis"
			"dynn_Artisiti"
			"dynn_Cholorionos"
			"dynn_Archodolopolos"
			"dynn_Archforos"
			"dynn_Diakanos"
			"dynn_Dimidios"
			"dynn_Drakonn"
			"dynn_Drakonos"
			"dynn_Economos"
			"dynn_Gatakios"
			"dynn_Haspanos"
			"dynn_Hasapanion"
			"dynn_Hatizin"
			"dynn_Iordikano"
			"dynn_Katopon"
			"dynn_Kampanarios"
			"dynn_Kallierges"
			"dynn_Loukites"
			"dynn_Kometopoulos"
			"dynn_Pateranos"
			"dynn_Paleologo"
			"dynn_Gregoras"
			"dynn_Sanudo"
			"dynn_Komytzes"
			"dynn_Lascaris"
			"dynn_Attikos"
			"dynn_Anastasiotis"
			"dynn_Evergetidiotis"
			"dynn_Chliarinos"
			"dynn_Chysovergis"
			"dynn_Eugeniotis"
			"dynn_Vorradiotis"
			"dynn_Mountanis"
			"dynn_Theotokitis"
			"dynn_Viticlenou"
			"dynn_Autoreianos"
			"dynn_Akoimitos"
			"dynn_Charitopoulos"
			"dynn_Panemorphiotis"
			"dynn_Hyakinthiotis"
			"dynn_Galesiotis"
			"dynn_Bekkos"
			"dynn_Kyprios"
			"dynn_Gonitis"
			"dynn_Pammakaristiotis"
			"dynn_Perivleptiotis"
			"dynn_Manganiotis"
			"dynn_Kalekas"
			"dynn_Xylaloes"
			"dynn_Syrikos"
			"dynn_Tatikios"
			"dynn_Aspietes"			
		}

		dynasty_names = {
			"dynn_Adephmos"
			"dynn_Aeteos"
			"dynn_Avalanthangelos"
			"dynn_Andinenvo"
			"dynn_Avariaknakis"
			"dynn_Artisiti"
			"dynn_Cholorionos"
			"dynn_Archodolopolos"
			"dynn_Archforos"
			"dynn_Diakanos"
			"dynn_Dimidios"
			"dynn_Drakonn"
			"dynn_Drakonos"
			"dynn_Economos"
			"dynn_Gatakios"
			"dynn_Haspanos"
			"dynn_Hasapanion"
			"dynn_Hatizin"
			"dynn_Iordikano"
			"dynn_Katopon"
			"dynn_Kampanarios"
			"dynn_Kallierges"
			"dynn_Loukites"
			"dynn_Kometopoulos"
			"dynn_Pateranos"
			"dynn_Paleologo"
			"dynn_Gregoras"
			"dynn_Sanudo"
			"dynn_Komytzes"
			"dynn_Lascaris"
			"dynn_Attikos"
			"dynn_Anastasiotis"
			"dynn_Evergetidiotis"
			"dynn_Chliarinos"
			"dynn_Chysovergis"
			"dynn_Eugeniotis"
			"dynn_Vorradiotis"
			"dynn_Mountanis"
			"dynn_Theotokitis"
			"dynn_Viticlenou"
			"dynn_Autoreianos"
			"dynn_Akoimitos"
			"dynn_Charitopoulos"
			"dynn_Panemorphiotis"
			"dynn_Hyakinthiotis"
			"dynn_Galesiotis"
			"dynn_Bekkos"
			"dynn_Kyprios"
			"dynn_Gonitis"
			"dynn_Pammakaristiotis"
			"dynn_Perivleptiotis"
			"dynn_Manganiotis"
			"dynn_Kalekas"
			"dynn_Xylaloes"
			"dynn_Syrikos"
			"dynn_Tatikios"
			"dynn_Aspietes"			
		}

		#male_names = { #Note, names added in section below. Kept this so custom names wern't forgotten among the others.
		#	Aeschratios Briarius Dryanius Kimonion
		#}
		
		male_names = {
			Streder Zaron Dantes Orfos Mon Quwas Zull Zuln Zult Thord Thorne Grasser Labynt Breyc Vorkan Salvorn Semid Irvis Hazen Lexn Fisn Cyn Drem Wedec Blane Byd Klysx Shaxarn Armens Serap Carm Acutun Argun Valarox Forgann Lorr Lorm Lerd Shu Casr Twax Farhaxx Neorn
		}
		
		#female_names = { #Note, names added in section below. Kept this so custom names wern't forgotten among the others.
		#	Auxesia Euphrosyne Timandaria
		#}
		
		female_names = {
			Streder Zaron Dantes Orfos Mon Quwas Zull Zuln Zult Thord Thorne Grasser Labynt Breyc Vorkan Salvorn Semid Irvis Hazen Lexn Fisn Cyn Drem Wedec Blane Byd Klysx Shaxarn Armens Serap Carm Acutun Argun Valarox Forgann Lorr Lorm Lerd Shu Casr Twax Farhaxx Neorn
		}

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			10 = byzantine
			30 = caucasian_northern_blond_fogeater
		}
	}
}

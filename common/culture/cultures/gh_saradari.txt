﻿gh_saradari_group = {
	graphical_cultures = {
		indo_aryan_group_coa_gfx
		indian_building_gfx
		indian_clothing_gfx
		indian_unit_gfx
	}
	mercenary_names = {
	
	}

	auzangeb = {
		
		color = { 0.45 0.0 0.0 }
		
		cadet_dynasty_names = { ####dynasties from vanilla indo-aryan-should get changed at some point
			"dynn_Jankahar"  
			"dynn_Parvatadvarka"
			"dynn_Sura"
			"dynn_Sharabhapuriya"
			"dynn_Mathara"
			"dynn_Vigraha"
			"dynn_Mudgalas"
			"dynn_Durjaya"
			"dynn_Shailodbhava"
			"dynn_Bhaumakara"
			"dynn_Somvanshi"
			"dynn_Cindaka_Naga"			
		}

		dynasty_names = {
			"dynn_Bali"
			"dynn_Parvatadvarka"
			"dynn_Sura"
			"dynn_Sharabhapuriya"
			"dynn_Mathara"
			"dynn_Vigraha"
			"dynn_Mudgalas"
			"dynn_Durjaya"
			"dynn_Shailodbhava"
			"dynn_Bhaumakara"
			"dynn_Somvanshi"
			"dynn_Cindaka_Naga"			
		}

		male_names = {
			Amritnarayan Balabhata Balinarayan Ballalsen Basudevnayaran
			Bhagadatta Bhairabendranarayan Bhatta Bhavashankari Bhimsingha Birnayaran
			Chandrasen Chandranarayan Chaturanan Chilarai
			Danujmadhava Dasharathadeva Devakhadga Devapala Devendranarayan
			Dhairjendranayaran Dharendranarayan Dharmapala Dhirsingha
			Gajnarayan Gopala Govindachandra Govindapala
			Hajo Harendranarayan Haridevnarayan Hariyamandal Hemantasen Indranarayan
			Jadu Jagaddipendranarayan Jatakhadga Jayanarayan Jayapala Jitendranarayan
			Kalyanachandra Kamalnarayan Keshabsen Khadgodyama Krishanmishra Krishnaroy
			Kumarapala Kumudnarayan Ladahachandra Lakshmansen Lakshmichandra Lakshminarayan Lomapada
			Madanapala Mahendrapala Mahendrasingha Mahindranarayan Mahipala Manava Mukundanarayan
			Naranarayan Narasingha Narayanapala Narendranarayan Nayapala Nripendranarayan
			Parikshitnayaran Prananarayan Prannath Pratapnarayan Pratapnarayanroy Pushkaranadhipa
			Raghudev Rajabhata Rajadinaj Rajaganesha Rajendranarayan Rajrajendranarayan Rajyapala
			Ramapala Ramnath Ranasura Ranjitsingha Roopnarayan Rudranarayan
			Samudrasen Shanibhangar Shashanka Shivanarayan Shivendranarayan Shukladhwaj Shurapala
			Srichandra Srimanta Subarnachandra Sukhdev Suryanarayan Traillokyachandra
			Udirnakhadga Uditanarayan Upendranayaran Vakapala Vapyata Vigrahapala
			Vijaynarayan Vijaysen Vijaysingha Virajdendranarayan Vishwarupsen Vishwasingha
			"Bhairav_Singh" "Bhavesh" "Bhogishwar" "Dev_Singh" "Dhir_Singh" Dhireshwar
			"Ganeshwar_Singh" "Gang_Dev" Harasimhadeva "Hari_Singh" "Hemant_Sen"
			"Kameshwar" "Kirti_Singh" Jyotirishwar "Laxman_Sen" "Laxminath_Singh_Dev" Madanpal
			"Nanya_Dev" "Nar_Singh" "Narsingh_Dev" "Padma_Singh" "Rambhadra_Singh_Dev" Rameshwar
			"Samant_Sen" "Shakrasingh_Dev" "Shiv_Singh"  "Vallal_Sen" "Vijay_Sen"
			Agnimitra Andhraka Ayus Bhagabhadra Bhikhari Bhumimitra Devabhuti Ghosha Gopal Govindapal
			Khsetravridha Maharshi Mahendra Maurayadhwaj Narayana Pulindaka Pusyamitra
			Sahtiya Susarman Vajramitra Vasudeva Vasujyeshtha Vasumitra
		}
		female_names = {
			Amrapali Amritakala Asima Bhavashankari "Bishwas_Devi" Bhogavati Bibibai Dattadevi Devavati "Devi_Ahiylya" Gandharavati
			Gayatri Himadrija Hira Jira Kanchani Mahendrani Malavyadevi Nayanadevi Rani Ratnadevi Suvrata
			Syamadevi Vijnayavati Vina Yajnavati
		}

		dynasty_of_location_prefix = "dynnp_of"

		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			10 = indian
		}
	}
	
	mirzana_jahar = {
		
		color = { 0.8 0.4 0.0 }
		
		cadet_dynasty_names = {
			"dynn_Jankahar"
			"dynn_Parvatadvarka"
			"dynn_Sura"
			"dynn_Sharabhapuriya"
			"dynn_Mathara"
			"dynn_Vigraha"
			"dynn_Mudgalas"
			"dynn_Durjaya"
			"dynn_Shailodbhava"
			"dynn_Bhaumakara"
			"dynn_Somvanshi"
			"dynn_Cindaka_Naga"			
		}

		dynasty_names = {
			"dynn_Bali"
			"dynn_Parvatadvarka"
			"dynn_Sura"
			"dynn_Sharabhapuriya"
			"dynn_Mathara"
			"dynn_Vigraha"
			"dynn_Mudgalas"
			"dynn_Durjaya"
			"dynn_Shailodbhava"
			"dynn_Bhaumakara"
			"dynn_Somvanshi"
			"dynn_Cindaka_Naga"			
		}

		male_names = {
			Amritnarayan Balabhata Balinarayan Ballalsen Basudevnayaran
			Bhagadatta Bhairabendranarayan Bhatta Bhavashankari Bhimsingha Birnayaran
			Chandrasen Chandranarayan Chaturanan Chilarai
			Danujmadhava Dasharathadeva Devakhadga Devapala Devendranarayan
			Dhairjendranayaran Dharendranarayan Dharmapala Dhirsingha
			Gajnarayan Gopala Govindachandra Govindapala
			Hajo Harendranarayan Haridevnarayan Hariyamandal Hemantasen Indranarayan
			Jadu Jagaddipendranarayan Jatakhadga Jayanarayan Jayapala Jitendranarayan
			Kalyanachandra Kamalnarayan Keshabsen Khadgodyama Krishanmishra Krishnaroy
			Kumarapala Kumudnarayan Ladahachandra Lakshmansen Lakshmichandra Lakshminarayan Lomapada
			Madanapala Mahendrapala Mahendrasingha Mahindranarayan Mahipala Manava Mukundanarayan
			Naranarayan Narasingha Narayanapala Narendranarayan Nayapala Nripendranarayan
			Parikshitnayaran Prananarayan Prannath Pratapnarayan Pratapnarayanroy Pushkaranadhipa
			Raghudev Rajabhata Rajadinaj Rajaganesha Rajendranarayan Rajrajendranarayan Rajyapala
			Ramapala Ramnath Ranasura Ranjitsingha Roopnarayan Rudranarayan
			Samudrasen Shanibhangar Shashanka Shivanarayan Shivendranarayan Shukladhwaj Shurapala
			Srichandra Srimanta Subarnachandra Sukhdev Suryanarayan Traillokyachandra
			Udirnakhadga Uditanarayan Upendranayaran Vakapala Vapyata Vigrahapala
			Vijaynarayan Vijaysen Vijaysingha Virajdendranarayan Vishwarupsen Vishwasingha
			"Bhairav_Singh" "Bhavesh" "Bhogishwar" "Dev_Singh" "Dhir_Singh" Dhireshwar
			"Ganeshwar_Singh" "Gang_Dev" Harasimhadeva "Hari_Singh" "Hemant_Sen"
			"Kameshwar" "Kirti_Singh" Jyotirishwar "Laxman_Sen" "Laxminath_Singh_Dev" Madanpal
			"Nanya_Dev" "Nar_Singh" "Narsingh_Dev" "Padma_Singh" "Rambhadra_Singh_Dev" Rameshwar
			"Samant_Sen" "Shakrasingh_Dev" "Shiv_Singh"  "Vallal_Sen" "Vijay_Sen"
			Agnimitra Andhraka Ayus Bhagabhadra Bhikhari Bhumimitra Devabhuti Ghosha Gopal Govindapal
			Khsetravridha Maharshi Mahendra Maurayadhwaj Narayana Pulindaka Pusyamitra
			Sahtiya Susarman Vajramitra Vasudeva Vasujyeshtha Vasumitra
		}
		female_names = {
			Amrapali Amritakala Asima Bhavashankari "Bishwas_Devi" Bhogavati Bibibai Dattadevi Devavati "Devi_Ahiylya" Gandharavati
			Gayatri Himadrija Hira Jira Kanchani Mahendrani Malavyadevi Nayanadevi Rani Ratnadevi Suvrata
			Syamadevi Vijnayavati Vina Yajnavati
		}

		dynasty_of_location_prefix = "dynnp_of"

		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			10 = indian
		}
	}
	
	babura = {
		
		color = { 0.6 0.3 0.0 }
		
		cadet_dynasty_names = {
			"dynn_Jankahar"
			"dynn_Parvatadvarka"
			"dynn_Sura"
			"dynn_Sharabhapuriya"
			"dynn_Mathara"
			"dynn_Vigraha"
			"dynn_Mudgalas"
			"dynn_Durjaya"
			"dynn_Shailodbhava"
			"dynn_Bhaumakara"
			"dynn_Somvanshi"
			"dynn_Cindaka_Naga"			
		}

		dynasty_names = {
			"dynn_Bali"
			"dynn_Parvatadvarka"
			"dynn_Sura"
			"dynn_Sharabhapuriya"
			"dynn_Mathara"
			"dynn_Vigraha"
			"dynn_Mudgalas"
			"dynn_Durjaya"
			"dynn_Shailodbhava"
			"dynn_Bhaumakara"
			"dynn_Somvanshi"
			"dynn_Cindaka_Naga"			
		}

		male_names = {
			Amritnarayan Balabhata Balinarayan Ballalsen Basudevnayaran
			Bhagadatta Bhairabendranarayan Bhatta Bhavashankari Bhimsingha Birnayaran
			Chandrasen Chandranarayan Chaturanan Chilarai
			Danujmadhava Dasharathadeva Devakhadga Devapala Devendranarayan
			Dhairjendranayaran Dharendranarayan Dharmapala Dhirsingha
			Gajnarayan Gopala Govindachandra Govindapala
			Hajo Harendranarayan Haridevnarayan Hariyamandal Hemantasen Indranarayan
			Jadu Jagaddipendranarayan Jatakhadga Jayanarayan Jayapala Jitendranarayan
			Kalyanachandra Kamalnarayan Keshabsen Khadgodyama Krishanmishra Krishnaroy
			Kumarapala Kumudnarayan Ladahachandra Lakshmansen Lakshmichandra Lakshminarayan Lomapada
			Madanapala Mahendrapala Mahendrasingha Mahindranarayan Mahipala Manava Mukundanarayan
			Naranarayan Narasingha Narayanapala Narendranarayan Nayapala Nripendranarayan
			Parikshitnayaran Prananarayan Prannath Pratapnarayan Pratapnarayanroy Pushkaranadhipa
			Raghudev Rajabhata Rajadinaj Rajaganesha Rajendranarayan Rajrajendranarayan Rajyapala
			Ramapala Ramnath Ranasura Ranjitsingha Roopnarayan Rudranarayan
			Samudrasen Shanibhangar Shashanka Shivanarayan Shivendranarayan Shukladhwaj Shurapala
			Srichandra Srimanta Subarnachandra Sukhdev Suryanarayan Traillokyachandra
			Udirnakhadga Uditanarayan Upendranayaran Vakapala Vapyata Vigrahapala
			Vijaynarayan Vijaysen Vijaysingha Virajdendranarayan Vishwarupsen Vishwasingha
			"Bhairav_Singh" "Bhavesh" "Bhogishwar" "Dev_Singh" "Dhir_Singh" Dhireshwar
			"Ganeshwar_Singh" "Gang_Dev" Harasimhadeva "Hari_Singh" "Hemant_Sen"
			"Kameshwar" "Kirti_Singh" Jyotirishwar "Laxman_Sen" "Laxminath_Singh_Dev" Madanpal
			"Nanya_Dev" "Nar_Singh" "Narsingh_Dev" "Padma_Singh" "Rambhadra_Singh_Dev" Rameshwar
			"Samant_Sen" "Shakrasingh_Dev" "Shiv_Singh"  "Vallal_Sen" "Vijay_Sen"
			Agnimitra Andhraka Ayus Bhagabhadra Bhikhari Bhumimitra Devabhuti Ghosha Gopal Govindapal
			Khsetravridha Maharshi Mahendra Maurayadhwaj Narayana Pulindaka Pusyamitra
			Sahtiya Susarman Vajramitra Vasudeva Vasujyeshtha Vasumitra
		}
		female_names = {
			Amrapali Amritakala Asima Bhavashankari "Bishwas_Devi" Bhogavati Bibibai Dattadevi Devavati "Devi_Ahiylya" Gandharavati
			Gayatri Himadrija Hira Jira Kanchani Mahendrani Malavyadevi Nayanadevi Rani Ratnadevi Suvrata
			Syamadevi Vijnayavati Vina Yajnavati
		}

		dynasty_of_location_prefix = "dynnp_of"

		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			10 = indian
		}
	}
	
	jangkar = { #United
		
		color = { 0.8 0.3 0.2 }
		
		cadet_dynasty_names = {
			"dynn_Jankahar"
			"dynn_Parvatadvarka"
			"dynn_Sura"
			"dynn_Sharabhapuriya"
			"dynn_Mathara"
			"dynn_Vigraha"
			"dynn_Mudgalas"
			"dynn_Durjaya"
			"dynn_Shailodbhava"
			"dynn_Bhaumakara"
			"dynn_Somvanshi"
			"dynn_Cindaka_Naga"			
		}

		dynasty_names = {
			"dynn_Bali"
			"dynn_Parvatadvarka"
			"dynn_Sura"
			"dynn_Sharabhapuriya"
			"dynn_Mathara"
			"dynn_Vigraha"
			"dynn_Mudgalas"
			"dynn_Durjaya"
			"dynn_Shailodbhava"
			"dynn_Bhaumakara"
			"dynn_Somvanshi"
			"dynn_Cindaka_Naga"			
		}

		male_names = {
			Amritnarayan Balabhata Balinarayan Ballalsen Basudevnayaran
			Bhagadatta Bhairabendranarayan Bhatta Bhavashankari Bhimsingha Birnayaran
			Chandrasen Chandranarayan Chaturanan Chilarai
			Danujmadhava Dasharathadeva Devakhadga Devapala Devendranarayan
			Dhairjendranayaran Dharendranarayan Dharmapala Dhirsingha
			Gajnarayan Gopala Govindachandra Govindapala
			Hajo Harendranarayan Haridevnarayan Hariyamandal Hemantasen Indranarayan
			Jadu Jagaddipendranarayan Jatakhadga Jayanarayan Jayapala Jitendranarayan
			Kalyanachandra Kamalnarayan Keshabsen Khadgodyama Krishanmishra Krishnaroy
			Kumarapala Kumudnarayan Ladahachandra Lakshmansen Lakshmichandra Lakshminarayan Lomapada
			Madanapala Mahendrapala Mahendrasingha Mahindranarayan Mahipala Manava Mukundanarayan
			Naranarayan Narasingha Narayanapala Narendranarayan Nayapala Nripendranarayan
			Parikshitnayaran Prananarayan Prannath Pratapnarayan Pratapnarayanroy Pushkaranadhipa
			Raghudev Rajabhata Rajadinaj Rajaganesha Rajendranarayan Rajrajendranarayan Rajyapala
			Ramapala Ramnath Ranasura Ranjitsingha Roopnarayan Rudranarayan
			Samudrasen Shanibhangar Shashanka Shivanarayan Shivendranarayan Shukladhwaj Shurapala
			Srichandra Srimanta Subarnachandra Sukhdev Suryanarayan Traillokyachandra
			Udirnakhadga Uditanarayan Upendranayaran Vakapala Vapyata Vigrahapala
			Vijaynarayan Vijaysen Vijaysingha Virajdendranarayan Vishwarupsen Vishwasingha
			"Bhairav_Singh" "Bhavesh" "Bhogishwar" "Dev_Singh" "Dhir_Singh" Dhireshwar
			"Ganeshwar_Singh" "Gang_Dev" Harasimhadeva "Hari_Singh" "Hemant_Sen"
			"Kameshwar" "Kirti_Singh" Jyotirishwar "Laxman_Sen" "Laxminath_Singh_Dev" Madanpal
			"Nanya_Dev" "Nar_Singh" "Narsingh_Dev" "Padma_Singh" "Rambhadra_Singh_Dev" Rameshwar
			"Samant_Sen" "Shakrasingh_Dev" "Shiv_Singh"  "Vallal_Sen" "Vijay_Sen"
			Agnimitra Andhraka Ayus Bhagabhadra Bhikhari Bhumimitra Devabhuti Ghosha Gopal Govindapal
			Khsetravridha Maharshi Mahendra Maurayadhwaj Narayana Pulindaka Pusyamitra
			Sahtiya Susarman Vajramitra Vasudeva Vasujyeshtha Vasumitra
		}
		female_names = {
			Amrapali Amritakala Asima Bhavashankari "Bishwas_Devi" Bhogavati Bibibai Dattadevi Devavati "Devi_Ahiylya" Gandharavati
			Gayatri Himadrija Hira Jira Kanchani Mahendrani Malavyadevi Nayanadevi Rani Ratnadevi Suvrata
			Syamadevi Vijnayavati Vina Yajnavati
		}

		dynasty_of_location_prefix = "dynnp_of"

		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			10 = indian
		}
	}
	
}

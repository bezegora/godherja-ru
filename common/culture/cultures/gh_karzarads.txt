﻿gh_karzarads_group = {
	graphical_cultures = {
		western_coa_gfx
		western_building_gfx
		western_clothing_gfx
		western_unit_gfx
		}
		mercenary_names = {
	}
karzarads = {
		
		color = { 0.95 0.70 0.0 }
		
		cadet_dynasty_names = {
			"Shabaeva"
			"Xabibullina"
			"Aslan"
			"Shabayeva"
			"Derzhavin"
			"Grimatov"
			"Adrakhmanov"
			"Yusupov"
			"Cihanov"
			"Khamatov"
		}

		dynasty_names = {
			"Shabaeva"
			"Xabibullina"
			"Aslan"
			"Shabayeva"
			"Derzhavin"
			"Grimatov"
			"Adrakhmanov"
			"Yusupov"
			"Cihanov"
			"Khamatov"
		}

		male_names = {
			Timerkhan Sagdeev
			Moxammatsafa Safin
			Ruslan Nigmatulin
			Arslan Gizzatulla
			Alfrid Yarullin
			Fayzulla Kalimullin
			Azat Khusainov
			Gabdelxaliq Samitov
			Erik Rakhmatullin
			Zaki Khamatov
			Kamil Muratov
			Zofar Khafizullin
			Axmat Gizzatulla
			Altynbay Baibekov
			Rudolf Rachmaninoff
			Farhat Mirzayanov
			Toktamis Galimov
			Ansar Fayzullin
			Xabibulla Bogdanov
			Xabir Rakhmatullin
			Afsin Dogan 
			Hizir Ayaz
		}
		female_names = {
			Regina Cihanova
			Aygol Baibekova
			Dinara Gainutdin
			Alfia Yarullina
			Zifa Qutuy
			Maaryam Kalimullina
			Zahida Yarulina
			Dana Derzhavin
			Nurdjamal Nigmatulina
			Elmira Akhmadulina
			Bika Gareeva
			Culpan Karimova
			Cacak Bilalova
			Regina Kalimullina
			Aznagul Rachmaninoff
			Gaysa Ganeyeva
			Rezeda Utiasheva
			Nailya Tuqay
			Alina Amirxan
			Adela Zinnurova
			Firuze Samur
			Tura Irge
		}

		patronym_prefix_male = "dynnpat_pre_mac"
		patronym_prefix_male_vowel = "dynnpat_pre_vow_mag"
		patronym_prefix_female = "dynnpat_pre_nic"
		patronym_prefix_female_vowel = "dynnpat_pre_vow_nig"
		
		always_use_patronym = yes

		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = { 	
			5 = caucasian_blond
			45 = caucasian_ginger
			30 = caucasian_brown_hair
			20 = caucasian_dark_hair
		}#sjalvolki coalition culture#sjalvolki coalition culture
	}
}
﻿gh_marmaladid_group = {
	graphical_cultures = {
		arabic_group_coa_gfx
		mena_building_gfx
		mena_clothing_gfx
		mena_unit_gfx
		dde_abbasid_clothing_gfx
	}
	mercenary_names = {
	
	}

	utyalid = {
		
		color = { 0.80 0.67 0.24  }
		
		cadet_dynasty_names = { ###placeholder dyn from vanilla
			"dynn_Hassu"
			"dynn_Aznagid"
			"dynn_Wanurid"
			"dynn_Alivid"
			"dynn_Madinid"
			"dynn_Muttawakilid"
			"dynn_Mansurid"
			"dynn_Yazid"
			"dynn_Tremecen"
			"dynn_Ishaqid"
			"dynn_Yahyavid"
			"dynn_Bilalid"
			"dynn_Utmanid"
			"dynn_Tittawin"
			"dynn_Sebtaid"
			"dynn_Tariqid"
			"dynn_Muhajir"
			"dynn_Ladenid"
			"dynn_Hannachid"
			"dynn_Sabrid"
			"dynn_Safavid"
			"dynn_Sajjadid"
			"dynn_Saidid"
			"dynn_Sedkid"
			"dynn_Shakeelid"
			"dynn_Sharifid"
			"dynn_Shehzadid"
			"dynn_Sidqid"
			"dynn_Tahirid"
			"dynn_Talalid"
			"dynn_Medjerdid"
			"dynn_Tasneemid"
			"dynn_Tayebid"
			"dynn_Umarid"
			"dynn_Waheedid"
			"dynn_Wahidid"
			"dynn_Waleedid"
			"dynn_Wazirid"
			"dynn_Younisid"
			"dynn_Zafarid"
			"dynn_Zahirid"
			"dynn_Zakariyid"
			"dynn_Zakidid"
			"dynn_Zaydid"
			"dynn_Abdoid"
			"dynn_Abidin"
			"dynn_Adilid"
			"dynn_Balochid"
			"dynn_Bakkalid"
			"dynn_Mahmoudid"
			"dynn_Nadeerid"
			"dynn_Nadvid"
			"dynn_Najidid"
			"dynn_Naqvid"
			"dynn_Namjoid"
			"dynn_Anwarid"
			"dynn_Fawzid"
			"dynn_Nawfalid"
			"dynn_Abbasid"
			"dynn_Bari"
			"dynn_Isavid"			
		}

		dynasty_names = {
			"dynn_Wakarah"
			"dynn_Aznagid"
			"dynn_Wanurid"
			"dynn_Alivid"
			"dynn_Madinid"
			"dynn_Muttawakilid"
			"dynn_Mansurid"
			"dynn_Yazid"
			"dynn_Tremecen"
			"dynn_Ishaqid"
			"dynn_Yahyavid"
			"dynn_Bilalid"
			"dynn_Utmanid"
			"dynn_Tittawin"
			"dynn_Sebtaid"
			"dynn_Tariqid"
			"dynn_Muhajir"
			"dynn_Ladenid"
			"dynn_Hannachid"
			"dynn_Sabrid"
			"dynn_Safavid"
			"dynn_Sajjadid"
			"dynn_Saidid"
			"dynn_Sedkid"
			"dynn_Shakeelid"
			"dynn_Sharifid"
			"dynn_Shehzadid"
			"dynn_Sidqid"
			"dynn_Tahirid"
			"dynn_Talalid"
			"dynn_Medjerdid"
			"dynn_Tasneemid"
			"dynn_Tayebid"
			"dynn_Umarid"
			"dynn_Waheedid"
			"dynn_Wahidid"
			"dynn_Waleedid"
			"dynn_Wazirid"
			"dynn_Younisid"
			"dynn_Zafarid"
			"dynn_Zahirid"
			"dynn_Zakariyid"
			"dynn_Zakidid"
			"dynn_Zaydid"
			"dynn_Abdoid"
			"dynn_Abidin"
			"dynn_Adilid"
			"dynn_Balochid"
			"dynn_Bakkalid"
			"dynn_Mahmoudid"
			"dynn_Nadeerid"
			"dynn_Nadvid"
			"dynn_Najidid"
			"dynn_Naqvid"
			"dynn_Namjoid"
			"dynn_Anwarid"
			"dynn_Fawzid"
			"dynn_Nawfalid"
			"dynn_Abbasid"
			"dynn_Bari"
			"dynn_Isavid"			
		}

		male_names = {
			Abu-Bakr Aarif Abdul-Gafur Abdul-Jaleel Abdul-Qadir Abdul-Wahab Abdul Abdullah Aghlab Akin Ali Alim Aram Azam Bahir Burhanaddin
			Fadil Fadl Faruk Ghalib Hafiz Halil Hasan Husam Hussayn Ibrahim Idris Is_mail Isa Jabir Jalil Jibril Khaireddin Khalil Mahdi Mahmud
			Mansur Mirza Mubarak Muhammad Mukhtar Murad Musa Muslihiddin Muzaffaraddin Najib Nasr Nizam Qawurd Ramadan Sa_daddin Sadiq Sami Samir Shaiban
			Shamir Shujah Sulayman Talib Ubayd Uways Yahya Ya_qub Youkhanna Yusuf Zeyd
		}
		female_names = {
			Adila Amsha Asiya Faghira Habiba Hanifa Jahaira Kamala Layla Maryam Nafisa Nyawela Parand Parween Paymaneh Paywand Qamara Rafiqa Rasa Rashida Reshawna
			Saaman Sabba Saghar Sahba Sajida Samira Semeah Setara Shahrbano Shahzadah Shameem Shararah Sheeftah Sheeva Shogofa Shokouh Shola Sholah Simin Souzan
			Taliba Tanaz Taneen Yagana Yakta Yasmin Zaynab
		}

		dynasty_of_location_prefix = "dynnp_mn"

		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			10 = arab
		}
	}
	
	dawasid = {
		
		color = hsv360{ 25 75 63 }
		
		cadet_dynasty_names = {
			"dynn_Hassu"
			"dynn_Aznagid"
			"dynn_Wanurid"
			"dynn_Alivid"
			"dynn_Madinid"
			"dynn_Muttawakilid"
			"dynn_Mansurid"
			"dynn_Yazid"
			"dynn_Tremecen"
			"dynn_Ishaqid"
			"dynn_Yahyavid"
			"dynn_Bilalid"
			"dynn_Utmanid"
			"dynn_Tittawin"
			"dynn_Sebtaid"
			"dynn_Tariqid"
			"dynn_Muhajir"
			"dynn_Ladenid"
			"dynn_Hannachid"
			"dynn_Sabrid"
			"dynn_Safavid"
			"dynn_Sajjadid"
			"dynn_Saidid"
			"dynn_Sedkid"
			"dynn_Shakeelid"
			"dynn_Sharifid"
			"dynn_Shehzadid"
			"dynn_Sidqid"
			"dynn_Tahirid"
			"dynn_Talalid"
			"dynn_Medjerdid"
			"dynn_Tasneemid"
			"dynn_Tayebid"
			"dynn_Umarid"
			"dynn_Waheedid"
			"dynn_Wahidid"
			"dynn_Waleedid"
			"dynn_Wazirid"
			"dynn_Younisid"
			"dynn_Zafarid"
			"dynn_Zahirid"
			"dynn_Zakariyid"
			"dynn_Zakidid"
			"dynn_Zaydid"
			"dynn_Abdoid"
			"dynn_Abidin"
			"dynn_Adilid"
			"dynn_Balochid"
			"dynn_Bakkalid"
			"dynn_Mahmoudid"
			"dynn_Nadeerid"
			"dynn_Nadvid"
			"dynn_Najidid"
			"dynn_Naqvid"
			"dynn_Namjoid"
			"dynn_Anwarid"
			"dynn_Fawzid"
			"dynn_Nawfalid"
			"dynn_Abbasid"
			"dynn_Bari"
			"dynn_Isavid"			
		}

		dynasty_names = {
			"dynn_Wakarah"
			"dynn_Aznagid"
			"dynn_Wanurid"
			"dynn_Alivid"
			"dynn_Madinid"
			"dynn_Muttawakilid"
			"dynn_Mansurid"
			"dynn_Yazid"
			"dynn_Tremecen"
			"dynn_Ishaqid"
			"dynn_Yahyavid"
			"dynn_Bilalid"
			"dynn_Utmanid"
			"dynn_Tittawin"
			"dynn_Sebtaid"
			"dynn_Tariqid"
			"dynn_Muhajir"
			"dynn_Ladenid"
			"dynn_Hannachid"
			"dynn_Sabrid"
			"dynn_Safavid"
			"dynn_Sajjadid"
			"dynn_Saidid"
			"dynn_Sedkid"
			"dynn_Shakeelid"
			"dynn_Sharifid"
			"dynn_Shehzadid"
			"dynn_Sidqid"
			"dynn_Tahirid"
			"dynn_Talalid"
			"dynn_Medjerdid"
			"dynn_Tasneemid"
			"dynn_Tayebid"
			"dynn_Umarid"
			"dynn_Waheedid"
			"dynn_Wahidid"
			"dynn_Waleedid"
			"dynn_Wazirid"
			"dynn_Younisid"
			"dynn_Zafarid"
			"dynn_Zahirid"
			"dynn_Zakariyid"
			"dynn_Zakidid"
			"dynn_Zaydid"
			"dynn_Abdoid"
			"dynn_Abidin"
			"dynn_Adilid"
			"dynn_Balochid"
			"dynn_Bakkalid"
			"dynn_Mahmoudid"
			"dynn_Nadeerid"
			"dynn_Nadvid"
			"dynn_Najidid"
			"dynn_Naqvid"
			"dynn_Namjoid"
			"dynn_Anwarid"
			"dynn_Fawzid"
			"dynn_Nawfalid"
			"dynn_Abbasid"
			"dynn_Bari"
			"dynn_Isavid"			
		}

		male_names = {
			Abu-Bakr Aarif Abdul-Gafur Abdul-Jaleel Abdul-Qadir Abdul-Wahab Abdul Abdullah Aghlab Akin Ali Alim Aram Azam Bahir Burhanaddin
			Fadil Fadl Faruk Ghalib Hafiz Halil Hasan Husam Hussayn Ibrahim Idris Is_mail Isa Jabir Jalil Jibril Khaireddin Khalil Mahdi Mahmud
			Mansur Mirza Mubarak Muhammad Mukhtar Murad Musa Muslihiddin Muzaffaraddin Najib Nasr Nizam Qawurd Ramadan Sa_daddin Sadiq Sami Samir Shaiban
			Shamir Shujah Sulayman Talib Ubayd Uways Yahya Ya_qub Youkhanna Yusuf Zeyd
		}
		female_names = {
			Adila Amsha Asiya Faghira Habiba Hanifa Jahaira Kamala Layla Maryam Nafisa Nyawela Parand Parween Paymaneh Paywand Qamara Rafiqa Rasa Rashida Reshawna
			Saaman Sabba Saghar Sahba Sajida Samira Semeah Setara Shahrbano Shahzadah Shameem Shararah Sheeftah Sheeva Shogofa Shokouh Shola Sholah Simin Souzan
			Taliba Tanaz Taneen Yagana Yakta Yasmin Zaynab
		}

		dynasty_of_location_prefix = "dynnp_mn"

		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			10 = arab
		}
	}
	
	mutsartan = {
		
		color = hsv360{ 25 100 63 }
		
		cadet_dynasty_names = {
			"dynn_Hassu"
			"dynn_Aznagid"
			"dynn_Wanurid"
			"dynn_Alivid"
			"dynn_Madinid"
			"dynn_Muttawakilid"
			"dynn_Mansurid"
			"dynn_Yazid"
			"dynn_Tremecen"
			"dynn_Ishaqid"
			"dynn_Yahyavid"
			"dynn_Bilalid"
			"dynn_Utmanid"
			"dynn_Tittawin"
			"dynn_Sebtaid"
			"dynn_Tariqid"
			"dynn_Muhajir"
			"dynn_Ladenid"
			"dynn_Hannachid"
			"dynn_Sabrid"
			"dynn_Safavid"
			"dynn_Sajjadid"
			"dynn_Saidid"
			"dynn_Sedkid"
			"dynn_Shakeelid"
			"dynn_Sharifid"
			"dynn_Shehzadid"
			"dynn_Sidqid"
			"dynn_Tahirid"
			"dynn_Talalid"
			"dynn_Medjerdid"
			"dynn_Tasneemid"
			"dynn_Tayebid"
			"dynn_Umarid"
			"dynn_Waheedid"
			"dynn_Wahidid"
			"dynn_Waleedid"
			"dynn_Wazirid"
			"dynn_Younisid"
			"dynn_Zafarid"
			"dynn_Zahirid"
			"dynn_Zakariyid"
			"dynn_Zakidid"
			"dynn_Zaydid"
			"dynn_Abdoid"
			"dynn_Abidin"
			"dynn_Adilid"
			"dynn_Balochid"
			"dynn_Bakkalid"
			"dynn_Mahmoudid"
			"dynn_Nadeerid"
			"dynn_Nadvid"
			"dynn_Najidid"
			"dynn_Naqvid"
			"dynn_Namjoid"
			"dynn_Anwarid"
			"dynn_Fawzid"
			"dynn_Nawfalid"
			"dynn_Abbasid"
			"dynn_Bari"
			"dynn_Isavid"			
		}

		dynasty_names = {
			"dynn_Wakarah"
			"dynn_Aznagid"
			"dynn_Wanurid"
			"dynn_Alivid"
			"dynn_Madinid"
			"dynn_Muttawakilid"
			"dynn_Mansurid"
			"dynn_Yazid"
			"dynn_Tremecen"
			"dynn_Ishaqid"
			"dynn_Yahyavid"
			"dynn_Bilalid"
			"dynn_Utmanid"
			"dynn_Tittawin"
			"dynn_Sebtaid"
			"dynn_Tariqid"
			"dynn_Muhajir"
			"dynn_Ladenid"
			"dynn_Hannachid"
			"dynn_Sabrid"
			"dynn_Safavid"
			"dynn_Sajjadid"
			"dynn_Saidid"
			"dynn_Sedkid"
			"dynn_Shakeelid"
			"dynn_Sharifid"
			"dynn_Shehzadid"
			"dynn_Sidqid"
			"dynn_Tahirid"
			"dynn_Talalid"
			"dynn_Medjerdid"
			"dynn_Tasneemid"
			"dynn_Tayebid"
			"dynn_Umarid"
			"dynn_Waheedid"
			"dynn_Wahidid"
			"dynn_Waleedid"
			"dynn_Wazirid"
			"dynn_Younisid"
			"dynn_Zafarid"
			"dynn_Zahirid"
			"dynn_Zakariyid"
			"dynn_Zakidid"
			"dynn_Zaydid"
			"dynn_Abdoid"
			"dynn_Abidin"
			"dynn_Adilid"
			"dynn_Balochid"
			"dynn_Bakkalid"
			"dynn_Mahmoudid"
			"dynn_Nadeerid"
			"dynn_Nadvid"
			"dynn_Najidid"
			"dynn_Naqvid"
			"dynn_Namjoid"
			"dynn_Anwarid"
			"dynn_Fawzid"
			"dynn_Nawfalid"
			"dynn_Abbasid"
			"dynn_Bari"
			"dynn_Isavid"			
		}

		male_names = {
			Abu-Bakr Aarif Abdul-Gafur Abdul-Jaleel Abdul-Qadir Abdul-Wahab Abdul Abdullah Aghlab Akin Ali Alim Aram Azam Bahir Burhanaddin
			Fadil Fadl Faruk Ghalib Hafiz Halil Hasan Husam Hussayn Ibrahim Idris Is_mail Isa Jabir Jalil Jibril Khaireddin Khalil Mahdi Mahmud
			Mansur Mirza Mubarak Muhammad Mukhtar Murad Musa Muslihiddin Muzaffaraddin Najib Nasr Nizam Qawurd Ramadan Sa_daddin Sadiq Sami Samir Shaiban
			Shamir Shujah Sulayman Talib Ubayd Uways Yahya Ya_qub Youkhanna Yusuf Zeyd
		}
		female_names = {
			Adila Amsha Asiya Faghira Habiba Hanifa Jahaira Kamala Layla Maryam Nafisa Nyawela Parand Parween Paymaneh Paywand Qamara Rafiqa Rasa Rashida Reshawna
			Saaman Sabba Saghar Sahba Sajida Samira Semeah Setara Shahrbano Shahzadah Shameem Shararah Sheeftah Sheeva Shogofa Shokouh Shola Sholah Simin Souzan
			Taliba Tanaz Taneen Yagana Yakta Yasmin Zaynab
		}

		dynasty_of_location_prefix = "dynnp_mn"

		pat_grf_name_chance = 50
		mat_grf_name_chance = 5
		father_name_chance = 10
		
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 5

		ethnicities = {
			10 = arab
		}
	}
}

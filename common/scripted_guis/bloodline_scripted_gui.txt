﻿reorder_traits = {
	scope = character
	effect = {
		if = {
			limit = { has_trait_with_flag = bloodline }
			bloodline_effect_switch = { EFFECT = reorder_traits }
		}
	}
}

bloodline_cenware_founder = {
	scope = character
	is_valid = { bloodline_cenware_extra_inherit = yes }
	effect = {
		custom_description_no_bullet = {
			text = bloodline_founder
			subject = global_var:bloodline_cenware_founder
		}
	}
}
bloodline_nikaryin_founder = {
	scope = character
	is_valid = { bloodline_nikaryin_extra_inherit = yes }
	effect = {
		custom_description_no_bullet = {
			text = bloodline_founder
			subject = global_var:bloodline_nikaryin_founder
		}
	}
}
bloodline_phanagorax_founder = {
	scope = character
	is_valid = { bloodline_phanagorax_extra_inherit = yes }
	effect = {
		custom_description_no_bullet = {
			text = bloodline_founder
			subject = global_var:bloodline_phanagorax_founder
		}
	}
}
bloodline_diplomacy_skill_founder = {
	scope = character
	effect = {
		custom_description_no_bullet = {
			text = bloodline_founder
			subject = global_var:bloodline_diplomacy_skill_founder
		}
	}
}
bloodline_martial_skill_founder = {
	scope = character
	effect = {
		custom_description_no_bullet = {
			text = bloodline_founder
			subject = global_var:bloodline_martial_skill_founder
		}
	}
}
bloodline_stewardship_skill_founder = {
	scope = character
	effect = {
		custom_description_no_bullet = {
			text = bloodline_founder
			subject = global_var:bloodline_stewardship_skill_founder
		}
	}
}
bloodline_intrigue_skill_founder = {
	scope = character
	effect = {
		custom_description_no_bullet = {
			text = bloodline_founder
			subject = global_var:bloodline_intrigue_skill_founder
		}
	}
}
bloodline_learning_skill_founder = {
	scope = character
	effect = {
		custom_description_no_bullet = {
			text = bloodline_founder
			subject = global_var:bloodline_learning_skill_founder
		}
	}
}
bloodline_dhacixen_founder = {
	scope = character
	effect = {
		custom_description_no_bullet = {
			text = bloodline_founder
			subject = global_var:bloodline_dhacixen_founder
		}
	}
}
bloodline_aexionarax_founder = {
	scope = character
	effect = {
		custom_description_no_bullet = {
			text = bloodline_founder
			subject = global_var:bloodline_aexionarax_founder
		}
	}
}
bloodline_maklea_founder = {
	scope = character
	effect = {
		custom_description_no_bullet = {
			text = bloodline_founder
			subject = global_var:bloodline_maklea_founder
		}
	}
}
bloodline_nothyx_founder = {
	scope = character
	effect = {
		custom_description_no_bullet = {
			text = bloodline_founder
			subject = global_var:bloodline_nothyx_founder
		}
	}
}
bloodline_axiaothea_founder = {
	scope = character
	effect = {
		custom_description_no_bullet = {
			text = bloodline_founder
			subject = global_var:bloodline_axiaothea_founder
		}
	}
}
bloodline_aeschraes_founder = {
	scope = character
	effect = {
		custom_description_no_bullet = {
			text = bloodline_founder
			subject = global_var:bloodline_aeschraes_founder
		}
	}
}
bloodline_pothacleas_founder = {
    scope = character
    is_valid = { bloodline_pothacleas_extra_inherit = yes }
    effect = {
        custom_description_no_bullet = {
            text = bloodline_founder
            subject = global_var:bloodline_pothacleas_founder
        }
    }
}
bloodline_the_skull_taker_founder = {
	scope = character
	effect = {
		custom_description_no_bullet = {
			text = bloodline_founder
			subject = global_var:bloodline_the_skull_taker_founder
		}
	}
}
bloodline_gardfrei_founder = {
	scope = character
	effect = {
		custom_description_no_bullet = {
			text = bloodline_founder
			subject = global_var:bloodline_gardfrei_founder
		}
	}
}
bloodline_huegons_founder = {
	scope = character
	effect = {
		custom_description_no_bullet = {
			text = bloodline_founder
			subject = global_var:bloodline_huegons_founder
		}
	}
}
bloodline_the_good_king_founder = {
	scope = character
	effect = {
		custom_description_no_bullet = {
			text = bloodline_founder
			subject = global_var:bloodline_the_good_king_founder
		}
	}
}
bloodline_the_prophet_founder = {
	scope = character
	effect = {
		custom_description_no_bullet = {
			text = bloodline_founder
			subject = global_var:bloodline_the_prophet_founder
		}
	}
}
bloodline_the_black_sun_founder = {
	scope = character
	effect = {
		custom_description_no_bullet = {
			text = bloodline_founder
			subject = global_var:bloodline_the_black_sun_founder
		}
	}
}
bloodline_kathanouxa_founder = {
	scope = character
	effect = {
		custom_description_no_bullet = {
			text = bloodline_founder
			subject = global_var:bloodline_kathanouxa_founder
		}
	}
}
bloodline_odyr_then_founder = {
	scope = character
	effect = {
		custom_description_no_bullet = {
			text = bloodline_founder
			subject = global_var:bloodline_odyr_then_founder
		}
	}
}
bloodline_odyr_lleth_founder = {
	scope = character
	effect = {
		custom_description_no_bullet = {
			text = bloodline_founder
			subject = global_var:bloodline_odyr_lleth_founder
		}
	}
}
bloodline_odyr_hune_founder = {
	scope = character
	effect = {
		custom_description_no_bullet = {
			text = bloodline_founder
			subject = global_var:bloodline_odyr_hune_founder
		}
	}
}
bloodline_vercigex_founder = {
	scope = character
	effect = {
		custom_description_no_bullet = {
			text = bloodline_founder
			subject = global_var:bloodline_vercigex_founder
		}
	}
}
bloodline_cois_defeated_founder = {
	scope = character
	effect = {
		custom_description_no_bullet = {
			text = bloodline_founder
			subject = global_var:bloodline_cois_defeated_founder
		}
	}
}
bloodline_cois_victorious_founder = {
	scope = character
	effect = {
		custom_description_no_bullet = {
			text = bloodline_founder
			subject = global_var:bloodline_cois_defeated_founder
		}
	}
}
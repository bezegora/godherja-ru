#########################################################################################################
#                                      CPR'S JANKY BUT FUCTIONAL...                                     #
#           _____ _______ _____ ______      _____ _______    _______     _______ _______ ______ __  __  #
#     /\   |  __ \__   __|_   _|  ____/\   / ____|__   __|  / ____\ \   / / ____|__   __|  ____|  \/  | #
#    /  \  | |__) | | |    | | | |__ /  \ | |       | |    | (___  \ \_/ / (___    | |  | |__  | \  / | #
#   / /\ \ |  _  /  | |    | | |  __/ /\ \| |       | |     \___ \  \   / \___ \   | |  |  __| | |\/| | #
#  / ____ \| | \ \  | |   _| |_| | / ____ \ |____   | |     ____) |  | |  ____) |  | |  | |____| |  | | #
# /_/    \_\_|  \_\ |_|  |_____|_|/_/    \_\_____|  |_|    |_____/   |_| |_____/   |_|  |______|_|  |_| #
#                                                                                                       #
#########################################################################################################

Hello, and welcome to my guide on how the artifact system functions, and how you can customize it to your needs.

Of course, the system isn't perfect, but it is 100% functional in its current state. I would've preferred to use some kind of a list or grid to display the artifacts, but it seems that no one has cracked how those work yet, or at least, no one I've seen. This project will likely be updated in the future once we learn how to make grids functional. Either way, onto how the system works.

Part 1 - The interface (gui\hud.gui)
For whatever reason, it currently isn't possible to make entirely new .gui files (or so I've heard), so Custom GUIs need to be tacked onto existing ones. I've add the artifacts system to hud.gui, at the very start of the topbar widget. Everything up to the artifacts_main_container can be ignored if you just want to add your own artifacts, as the code before it sets up the upper part of the window, which doesn't need to be touched. After this part is where the actual artifact entries start (and a textbox for when you have no artifacts). The entries look like this:

vbox = {
	name = "artifact_1"
	datacontext = "[GetScriptedGui('artifacts_scripted_artifact_1')]"
	visible = "[ScriptedGui.IsShown(GuiScope.SetRoot(GetPlayer.MakeScope).End)]"
	
	button_standard = {
		size = { 500 100 }
		maximumsize = { 500 100 }
		
		datacontext = "[GetScriptedGui('artifacts_scripted_artifact_1')]"
		onclick = "[ScriptedGui.Execute(GuiScope.SetRoot(GetPlayer.MakeScope).End)]"
		
		block "artifact_entry"
		{}
		hbox = {
			background = {
				texture = "gfx/interface/tiles/tile_selected.dds"
				spriteType = Corneredtiled
				spriteborder = { 6 6 }
				shaderfile = "gfx/FX/pdxgui_default.shader"
				block "artifact_entry_border"
				{}
			}
	
			button = {
				size = { 100 100 }
				alwaystransparent = yes
				background = {
					texture = "gfx/interface/tiles/tile_selected.dds"
					spriteType = Corneredtiled
					spriteborder = { 6 6 }
					shaderfile = "gfx/FX/pdxgui_default.shader"
					block "artifact_entry_border"
					{}
				}
				icon = {
					texture = "gfx/artifacts/artifact_blacksunmask.dds"
					size = { 100 100 }
					parentanchor = center
				}
				icon = {
					texture = "gfx/artifacts/artifacts_entry_bg.dds"
					position = { 100 2 }
					alwaystransparent = yes
					layer = bottom
				}
			}
			artifact_spacer_horizontal = {}
			
			vbox = {
				artifact_spacer_vertical = {}
				layoutpolicy_horizontal = expanding
				layoutpolicy_vertical = expanding
				hbox = {
					layoutpolicy_horizontal = expanding
					textbox = {
						font = StandardGameFont
						fontcolor = { 1 1 1 1 }
						fontsize = 16
						text = artifact_1_name
						autoresize = no
						layoutpolicy_horizontal = expanding
						elide = right
						minimumsize = { -1 16 }
						align = left
						block "artifact_name"
						{}
					}
					textbox = {
						font = StandardGameFont
						fontcolor = { 1 1 1 1 }
						fontsize = 16
						text = artifact_rarity
						autoresize = no
						layoutpolicy_horizontal = expanding
						elide = right
						minimumsize = { -1 16 }
						align = right
					}
					artifact_spacer_horizontal = {}
					icon = {
						texture = "gfx/artifacts/icon_check.dds"
						size = { 22 16 }
						datacontext = "[GetScriptedGui('artifacts_equipped_artifact_1')]"
						visible = "[ScriptedGui.IsShown(GuiScope.SetRoot(GetPlayer.MakeScope).End)]"
					}
					artifact_spacer_horizontal = {}
				}
				artifact_spacer_vertical = {}
				textbox = {
					font = StandardGameFont
					fontcolor = { 0.8 0.8 0.8 1 }
					fontsize = 12
					text = artifact_1_stats
					autoresize = no
					layoutpolicy_horizontal = expanding
					layoutpolicy_vertical = expanding
					elide = right
					multiline = yes
					align = top|left
					block "artifact_description"
					{}
				}
				artifact_spacer_vertical = {}
			}
		}
	}
}

Large, I know. You should only need to edit a few pieces of this entry, though. First, texture = "gfx/artifacts/artifact_blacksunmask.dds" sets the image of the artifact. This can be changed by simply adjust the path to your artifact's image. I'd recommended keeping them all in the same folder for neatness. Next up, text = artifact_1_name is the name of the artifact. All you've got to do it make a new loc key for your artifact, which I'll cover later. Finally, artifact_1_stats is the loc key that contains the statistics/buffs/effects from the artifact. Like with the name, it's just a loc key. There are also several scripted GUIs in this block, including artifacts_equipped_artifact_1 and artifacts_scripted_artifact_1.
When you make a new artifact, copy this block (or, preferably, the one already in hud.gui) and replace artifact_# with the new number of your artifact. Make sure it is not a repeat or the system will likely break. So, if my last artifact is artifact_10, when I make a new artifact, I would change every occurrence of artifact_10 to artifact_11.

Part 2 - The Scripted GUI (common\scripted_guis\gh_artifacts_scripted_gui.txt)
This is where the magic happens, kinda. If it were HOI4 then yeah, this is where the magic happens, but CK3 moved a lot of the magic to the .gui file, so the scripted GUI is a lot less impressive. When you make a new artifact, you need to update the 3 buttons with the new artifact's variable, and add two new scripted GUIs for the artifact. The artifact's scripted GUIs looks like this:

artifacts_scripted_artifact_1 = {
	scope = character
	
	is_shown = {
		has_variable = has_artifact_1
	}
	
	effect = {
		clear_all_artifacts = yes
		set_variable = selected_artifact_1
	}
}

artifacts_equipped_artifact_1 = {
	scope = character
	
	is_shown = {
		has_variable = equipped_artifact_1
	}
}

Pretty simple. Just copy and update the artifact number to your new artifact. The buttons are also listed in the same file, and I will not be copying those here. Like with every other part of the system, if you're adding a new artifact, copy an existing artifact entry in the button and update it to the new number. Make sure to update all 3 buttons, or you won't be able to interact with your artifacts. Additionally, this file also contains the actual effects of the artifact, whether it be a trait or a stat boost or something else. There are comments left in the file where you add these effects. I would recommended using character modifiers for artifact effects, or having them add traits while the artifacts are equipped. Of course, you're free to get creative with it.

Part 3 - The Localization (localization\english\gh_artifacts_loc_l_english.yml)

Very easy, each artifact has 3 pieces of loc with it:

artifact_1_name:0 "The Aversarian Crown"
artifact_1_desc:0 "The Crown of Aversaria has been worn by every reigning Aautokratir besides two for over a thousand years. Imposing and ornately designed, the crown gives off mixed feelings of fear and awe, and tales say that the heads of so many powerful magi have left the crown with its own magical aura."
artifact_1_stats:0 "[piety|E]: @piety_icon! #P +1.0#!/month\n[prestige|E]: @prestige_icon! #P +4.0#!/month\n#P +X#! magical power per month"

Additionally, if you are using character modifiers, you'll need to define the name and description for them as well (The stats for these are handled automatically):

artifact_1_modifier:0 "The Aversarian Crown"
artifact_1_modifier_desc:0 "The Crown of Aversaria has been worn by every reigning Aautokratir besides two for over a thousand years. Imposing and ornately designed, the crown gives off mixed feelings of fear and awe, and tales say that the heads of so many powerful magi have left the crown with its own magical aura."

Just copy an existing set of loc and change it to fit what you need. The loc keys should end up matching what you have in the hud.gui file artifact entries, as well. The stats in the stats loc key should reflect what the artifact does, unless you're a liar. The modifier name and description can be the same as the text shown in the GUI (which is what I did above), unless you want it to show something different.

Part 4 - The Custom Localization (common\customizable_localization\gh_artifacts_custom_loc.txt)

Another fairly easy step, this is what makes the text in the upper window change when you click an artifact (as well as some other misc text). Like with the buttons, I won't be copying it here, but when adding a new artifact, copy and existing loc key and trigger group, and update it with your new artifact number. Make sure to do this in the title, description, rarity, and equip scripted loc sections.

Part 5 - The Scripted Effects (common\scripted_effects\gh_artifacts_scripted_effects.txt)

There's only two scripted effects that really aren't needed but I'm keeping it around because short code is nice. Just copy a line and update it with the new artifact number for both effects. There is a third effect but it doesn't need to be touched.

Part 6 - Assigning Artifacts and on_actions (common\on_action\gh_on_actions.txt and elsewhere)

Artifacts are handled via character scoped variables, the most important of which is has_artifact_1 (or whatever number artifact you're working on). This variable can be set anywhere, as long as it is set in a character scope. For debugging, I gave one character all the artifacts at game start, using on_actions, like so:

on_game_start = { 
	effect = {
		character:sjalvolki_1 = {
			set_variable = has_artifact_1
			set_variable = has_artifact_2
			set_variable = has_artifact_3
			set_variable = has_artifact_4
			set_variable = has_artifact_5
			set_variable = has_artifact_6
			set_variable = has_artifact_7
			set_variable = has_artifact_8
			set_variable = has_artifact_9
			set_variable = has_artifact_10
		}
	}
}

You should be able to set this variable anywhere that variables can be set, however.
Additionally, there is also an on_death on_action that will handling transferring a characters artifacts on death. Being murder will give the artifacts to the murderer, while dying in an other way will pass the artifacts to the primary heir. Like with the other blocks of code, you should update this to account for new artifacts being added. If you don't like how this works, you are more than free to change what happens to a character's artifacts on death.

And that's the artifact system. Hopefully it works out well for you, and I hope this guide was easy enough to understand.
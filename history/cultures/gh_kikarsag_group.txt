1254.1.1 = {
	#Tribal Mil Innovations
	discover_innovation = innovation_motte
	discover_innovation = innovation_bannus
	discover_innovation = innovation_catapult
	discover_innovation = innovation_quilted_armor
	discover_innovation = innovation_barracks
	discover_innovation = innovation_mustering_grounds
	#Tribal Civ Innovations
	discover_innovation = innovation_development_01
	discover_innovation = innovation_ledger
	#Tribal MAA Innovations
	discover_innovation = innovation_regular_magic
}
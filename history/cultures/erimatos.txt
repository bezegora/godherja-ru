1250.1.1 = {
	#Tribal Mil Innovations
	discover_innovation = innovation_motte
	discover_innovation = innovation_bannus
	discover_innovation = innovation_catapult
	discover_innovation = innovation_quilted_armor
	discover_innovation = innovation_barracks
	discover_innovation = innovation_mustering_grounds
	#Tribal Civ Innovations
	discover_innovation = innovation_development_01
	discover_innovation = innovation_gavelkind
	discover_innovation = innovation_currency_01
	discover_innovation = innovation_crop_rotation
	discover_innovation = innovation_ledger
	discover_innovation = innovation_city_planning
	discover_innovation = innovation_plenary_assemblies
	discover_innovation = innovation_casus_belli
	#Tribal MAA Innovations
	discover_innovation = innovation_imperial_magic
	discover_innovation = innovation_regular_magic
	discover_innovation = innovation_duneriders
	#Era Unlock
	join_era = culture_era_early_medieval
}
1253.1.1 = {
	# Early Mil Innovations
	discover_innovation = innovation_burhs
	discover_innovation = innovation_battlements
	discover_innovation = innovation_mangonel
	discover_innovation = innovation_house_soldiers
	discover_innovation = innovation_arched_saddle
	# Early Civ Innovations
	discover_innovation = innovation_hereditary_rule
	discover_innovation = innovation_manorialism
	discover_innovation = innovation_development_02
	discover_innovation = innovation_currency_02
	discover_innovation = innovation_royal_prerogative
	discover_innovation = innovation_chronicle_writing
}
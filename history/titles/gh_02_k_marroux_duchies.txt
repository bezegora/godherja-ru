d_grascoi = {
	1215.1.1 = { 
		change_development_level = 12 
	}
}
d_prusca = {
	1215.1.1 = { 
		change_development_level = 14 
	}
}
d_varusiex = {	
	1192.4.15 = {
		holder = marcher_10500
	}
	1242.4.26 = {
		holder = marcher_530
	}
	1253.11.16 = {	
		liege = "k_marroux"
	}
	1253.11.16 = {
		change_development_level = 11
	}
}

d_cavrine = {	
	1192.4.15 = {
		holder = marcher_10500
	}
	1242.4.26 = {
		holder = marcher_530
	}
	1253.11.16 = {
		change_development_level = 12
	}
}

c_puiveres = {	
	1192.4.15 = {	
		liege = "k_marroux"
		holder = marcher_10500
	}
	1242.4.26 = {	
		liege = "k_marroux"
		holder = marcher_530
	}
	1215.1.1 = { 
		change_development_level = 11 
	}
}

c_veressirlaluc = {	
	1192.4.15 = {	
		liege = "k_marroux"
		holder = marcher_10500
	}
	1242.4.26 = {	
		liege = "k_marroux"
		holder = marcher_530
	}
	1215.1.1 = { 
		change_development_level = 11 
	}
}
d_duravux = {
	1215.1.1 = { 
		change_development_level = 12 
	}
}
d_marenoc = {
	1215.1.1 = { 
		change_development_level = 13 
	}
}
d_evrinesgoen = {	
	1253.11.16 = {	
		liege = "k_marroux"
		holder = marcher_535
	}
	1215.1.1 = { 
		change_development_level = 8 
	}
}
d_baresc = {	
	1253.11.16 = {	
		liege = "k_marroux"
		holder = marcher_535
	}
	1215.1.1 = { 
		change_development_level = 13 
	}
}
d_xaruns = {	
	1253.11.16 = {	
		liege = "k_marroux"
		holder = marcher_536
	}
	1215.1.1 = { 
		change_development_level = 14 
	}
}
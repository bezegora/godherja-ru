c_permapilese = {
	1215.1.1 = { change_development_level = 12 }
	
	1215.1.1 = {
		holder = marcher_577
		government = republic_government
		liege = 0
	}
}
c_nambre = {
	1215.1.1 = { change_development_level = 11 }
	
	1253.12.21 = {
		holder = marcher_4 ##renee##
		government = feudal_government
		liege = 0
	}
}
c_berion = {
	1253.1.1 = {
		holder = marcher_569
		liege = "d_bassons"
	}
}
c_maanasc = {
	1215.1.1 = {
		holder = marcher_11
	}
}
c_manavoi = {
	1196.2.13 = {
		holder = marcher_575
		liege = 0
	}
}
c_siconbre = {
	1228.3.14 = {
		holder = marcher_576
	}
}
c_fuscinaw = {
	1228.3.14 = {
		holder = marcher_576
	}
}
c_craocastinies = {
	1228.3.14 = {
		holder = marcher_576
	}
}
c_creprasc = {
	1253.1.1 = {
		holder = marcher_567
	}
}
c_galledoc = {
	1253.1.1 = {
		holder = marcher_568
	}
}
c_bacsi = {
	1253.11.16 = {
		liege = "k_varrdevet"
		holder = marcher_518
	}
}
c_bregef = {
	1253.11.16 = {
		liege = "k_varrdevet"
		holder = marcher_520
	}
}
c_paisano = {
	1253.11.16 = {
		liege = "k_varrdevet"
		holder = marcher_520
	}
}
c_niort = {
	1253.11.16 = {
		liege = "k_varrdevet"
		holder = marcher_521
	}
}
c_weversplit = {
	1253.11.16 = {
		liege = "k_varrdevet"
		holder = marcher_523
	}
}
c_chaudchien = {
	1253.11.16 = {
		liege = "k_varrdevet"
		holder = marcher_526
	}
}
c_geedern = {
	1253.11.16 = {
		liege = "k_varrdevet"
		holder = marcher_526
	}
}
c_tireic = {
	1253.11.16 = {
		liege = "k_varrdevet"
		holder = marcher_526
	}
}
c_baliec = {
	1253.11.16 = {
		liege = "k_varrdevet"
		holder = marcher_527
	}
}
c_vaduex = {
	1253.11.16 = {
		liege = "k_varrdevet"
		holder = marcher_527
	}
}
c_scoc = {
	1253.11.16 = {
		liege = "k_varrdevet"
		holder = marcher_529
	}
}
c_ouipaour = {
	1253.11.16 = {
		liege = "k_varrdevet"
		holder = marcher_529
	}
}
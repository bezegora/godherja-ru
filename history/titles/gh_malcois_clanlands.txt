
k_sanos = {
	1186.11.15 = {
		holder = western_imperial_1
	}
	1205.1.1 = {
		holder = 0
	}
	1250.1.1 = {
		succession_laws = { sjalvolki_succession_law }
	}
}

k_kallios = {
	1186.11.15 = {
		holder = western_imperial_1
	}
	1205.1.1 = {
		holder = 0
	}
	1250.1.1 = {
		succession_laws = { sjalvolki_succession_law }
	}
}

k_helenai = {
	1186.11.15 = {
		holder = western_imperial_1
	}
	1205.1.1 = {
		holder = 0
	}
	1250.1.1 = {
		succession_laws = { sjalvolki_succession_law }
	}
}

d_gelale = {
	1250.1.1 = {
		holder = clanlands_malcois_1
		liege = e_sjalvolki
		government = tribal_government
	}
	1215.1.1 = { change_development_level = 8}
}
c_nollidiri = {
	1250.1.1 = {
		holder = clanlands_malcois_2
		liege = d_gelale
		government = tribal_government
	}
}
c_dymanthus = {
	1250.1.1 = {
		holder = clanlands_malcois_2
		liege = d_gelale
		government = tribal_government
	}
}
c_epheis = {
	1250.1.1 = {
		holder = clanlands_malcois_3
		liege = d_gelale
		government = tribal_government
	}
}

d_addia = {
1215.1.1 = { change_development_level = 8}
}

c_abia = {
	1250.1.1 = {
		holder = clanlands_malcois_4
		liege = e_sjalvolki
		government = tribal_government
	}
}
c_knothos = {
	1250.1.1 = {
		holder = clanlands_malcois_4
		liege = e_sjalvolki
		government = tribal_government
	}
}
c_pyeis = {
	1250.1.1 = {
		holder = clanlands_malcois_4
		liege = e_sjalvolki
		government = tribal_government
	}
}
d_ithna = {
	1250.1.1 = {
		holder = clanlands_malcois_5
		liege = e_sjalvolki
		government = tribal_government
	}
	1215.1.1 = { change_development_level = 7}
}
d_knokidbes = {
	1250.1.1 = {
		holder = clanlands_malcois_5
		liege = e_sjalvolki
		government = tribal_government
	}
	1215.1.1 = { change_development_level = 7}
}
c_lytimis = {
	1250.1.1 = {
		holder = clanlands_malcois_6
		liege = d_ithna
		government = tribal_government
	}
}
c_ephsos = {
	1250.1.1 = {
		holder = clanlands_malcois_6
		liege = d_ithna
		government = tribal_government
	}
}
c_leios = {
	1250.1.1 = {
		holder = clanlands_malcois_7
		liege = d_ithna
		government = tribal_government
	}
}
c_prophaakike = {
	1250.1.1 = {
		holder = clanlands_malcois_8
		liege = e_sjalvolki
		government = tribal_government
	}
}
c_iola = {
	1250.1.1 = {
		holder = clanlands_malcois_8
		liege = e_sjalvolki
		government = tribal_government
	}
}
c_emos = {
	1250.1.1 = {
		holder = clanlands_malcois_8
		liege = e_sjalvolki
		government = tribal_government
	}
}
d_maadoran = {
	1250.1.1 = {
		holder = clanlands_malcois_9
		liege = e_sjalvolki
		government = tribal_government
	}
	1215.1.1 = { change_development_level = 8}
}
c_iorophos = {
	1250.1.1 = {
		holder = clanlands_malcois_10
		liege = d_maadoran
		government = tribal_government
	}
}
c_ithphaon = {
	1250.1.1 = {
		holder = clanlands_malcois_10
		liege = d_maadoran
		government = tribal_government
	}
}
c_elynos = {
	1250.1.1 = {
		holder = clanlands_malcois_11
		liege = d_maadoran
		government = tribal_government
	}
}
c_makos = {
	1250.1.1 = {
		holder = clanlands_malcois_11
		liege = d_maadoran
		government = tribal_government
	}
}
c_syrythna = {
	1250.1.1 = {
		holder = clanlands_malcois_11
		liege = d_maadoran
		government = tribal_government
	}
}
d_oradoth = {
	1250.1.1 = {
		holder = clanlands_malcois_12
		liege = e_sjalvolki
		government = tribal_government
	}
	1215.1.1 = { change_development_level = 11}
}
c_mepia = {
	1250.1.1 = {
		holder = clanlands_malcois_13
		liege = d_oradoth
		government = tribal_government
	}
}
c_othedryres = {
	1250.1.1 = {
		holder = clanlands_malcois_13
		liege = d_oradoth
		government = tribal_government
	}
}

d_vrekennus = {
1215.1.1 = { change_development_level = 8}
}
c_dykos = {
	1250.1.1 = {
		holder = clanlands_malcois_13
		liege = d_oradoth
		government = tribal_government
	}
}
c_pythria = {
	1250.1.1 = {
		holder = clanlands_malcois_14
		liege = d_oradoth
		government = tribal_government
	}
}
c_myma = {
	1250.1.1 = {
		holder = clanlands_malcois_15
		liege = e_sjalvolki
		government = tribal_government
	}
}
c_orontis = {
	1250.1.1 = {
		holder = clanlands_malcois_15
		liege = e_sjalvolki
		government = tribal_government
	}
}

d_oekevrokanne = {
1215.1.1 = { change_development_level = 8}
}
c_aste = {
	1250.1.1 = {
		holder = clanlands_malcois_16
		liege = e_sjalvolki
		government = tribal_government
	}
}
c_sys = {
	1250.1.1 = {
		holder = clanlands_malcois_16
		liege = e_sjalvolki
		government = tribal_government
	}
}
c_hyponnus = {
	1250.1.1 = {
		holder = clanlands_malcois_16
		liege = e_sjalvolki
		government = tribal_government
	}
}
d_dryn = {
1215.1.1 = { change_development_level = 9}
}
c_rhodroike = {
	1250.1.1 = {
		holder = clanlands_malcois_17
		liege = e_sjalvolki
		government = tribal_government
	}
}


k_kallios = {
	1250.1.1 = {
		holder = clanlands_malcois_18
		liege = e_sjalvolki
		government = tribal_government
	}
}
d_saris = {
	1250.1.1 = {
		holder = clanlands_malcois_19
		liege = k_kallios
		government = tribal_government
	}
	1215.1.1 = { change_development_level = 6}
}
c_oedes = {
	1250.1.1 = {
		holder = clanlands_malcois_20
		liege = d_saris
		government = tribal_government
	}
}
c_olymis = {
	1250.1.1 = {
		holder = clanlands_malcois_21
		liege = d_saris
		government = tribal_government
	}
}

d_olythssoessa = {
1215.1.1 = { change_development_level = 6}
}
c_anae = {
	1250.1.1 = {
		holder = clanlands_malcois_22
		liege = k_kallios
		government = tribal_government
	}
}
c_lephbes = {
	1250.1.1 = {
		holder = clanlands_malcois_23
		liege = k_kallios
		government = tribal_government
	}
}
c_cupus = {
	1250.1.1 = {
		holder = clanlands_malcois_24
		liege = k_kallios
		government = tribal_government
	}
}
c_chin = {
	1250.1.1 = {
		holder = clanlands_malcois_24
		liege = k_kallios
		government = tribal_government
	}
}
c_rhoea = {
	1250.1.1 = {
		holder = clanlands_malcois_25
		liege = k_kallios
		government = tribal_government
	}
}

d_prirkos = {
1215.1.1 = { change_development_level = 5}
}
c_chassora = {
	1250.1.1 = {
		holder = clanlands_malcois_26
		liege = k_kallios
		government = tribal_government
	}
}
c_chila = {
	1250.1.1 = {
		holder = clanlands_malcois_26
		liege = k_kallios
		government = tribal_government
	}
}
c_orcelephnae = {
	1250.1.1 = {
		holder = clanlands_malcois_27
		liege = k_kallios
		government = tribal_government
	}
}

d_elrhyos = {
1215.1.1 = { change_development_level = 9}
}
c_gephadike = {
	1250.1.1 = {
		holder = clanlands_malcois_28
		liege = k_kallios
		government = tribal_government
	}
}
c_rhece = {
	1250.1.1 = {
		holder = clanlands_malcois_29
		liege = e_sjalvolki
		government = tribal_government
	}
}
c_thete = {
	1250.1.1 = {
		holder = clanlands_malcois_30
		liege = e_sjalvolki
		government = tribal_government
	}
}
d_askigos = {
	1250.1.1 = {
		holder = clanlands_malcois_31
		liege = e_sjalvolki
		government = tribal_government
	}
	1215.1.1 = { change_development_level = 9}
}
d_oedi = {
	1250.1.1 = {
		holder = clanlands_malcois_31
		liege = e_sjalvolki
		government = tribal_government
	}
	1215.1.1 = { change_development_level = 10}
}
c_posike = {
	1250.1.1 = {
		holder = clanlands_malcois_32
		liege = d_askigos
		government = tribal_government
	}
}
c_taecos = {
	1250.1.1 = {
		holder = clanlands_malcois_32
		liege = d_askigos
		government = tribal_government
	}
}
c_anphious = {
	1250.1.1 = {
		holder = clanlands_malcois_33
		liege = d_askigos
		government = tribal_government
	}
}
c_elssoth = {
	1250.1.1 = {
		holder = clanlands_malcois_34
		liege = e_sjalvolki
		government = tribal_government
	}
}
c_hiereutherna = {
	1250.1.1 = {
		holder = clanlands_malcois_34
		liege = e_sjalvolki
		government = tribal_government
	}
}
c_tion = {
	1250.1.1 = {
		holder = clanlands_malcois_35
		liege = e_sjalvolki
		government = tribal_government
	}
}

d_iopolos = {
1215.1.1 = { change_development_level = 8}
}
c_hath = {
	1250.1.1 = {
		holder = clanlands_malcois_35
		liege = e_sjalvolki
		government = tribal_government
	}
}
c_karrhythus = {
	1250.1.1 = {
		holder = clanlands_malcois_35
		liege = e_sjalvolki
		government = tribal_government
	}
}
c_lyth = {
	1250.1.1 = {
		holder = clanlands_malcois_36
		liege = e_sjalvolki
		government = tribal_government
	}
}

d_kethetis = {
1215.1.1 = { change_development_level = 6}
}
c_lefmis = {
	1250.1.1 = {
		holder = clanlands_malcois_37
		liege = e_sjalvolki
		government = tribal_government
	}
}

d_adne = {
1215.1.1 = { change_development_level = 5}
}
c_knoike = {
	1250.1.1 = {
		holder = clanlands_malcois_38
		liege = e_sjalvolki
		government = tribal_government
	}
}
c_merarhyla = {
	1250.1.1 = {
		holder = clanlands_malcois_39
		liege = e_sjalvolki
		government = tribal_government
	}
}
d_abene = {
1215.1.1 = { change_development_level = 8}
}
c_messis = {
	1250.1.1 = {
		holder = clanlands_malcois_39
		liege = e_sjalvolki
		government = tribal_government
	}
}
c_saadgos = {
	1250.1.1 = {
		holder = clanlands_malcois_39
		liege = e_sjalvolki
		government = tribal_government
	}
}
c_teus = {
	1250.1.1 = {
		holder = clanlands_malcois_40
		liege = e_sjalvolki
		government = tribal_government
	}
}

d_grylothephon = {
1215.1.1 = { change_development_level = 6}
}
c_assoesnai = {
	1250.1.1 = {
		holder = clanlands_malcois_40
		liege = e_sjalvolki
		government = tribal_government
	}
}
d_oios = {
	1250.1.1 = {
		holder = clanlands_malcois_41
		liege = e_sjalvolki
		government = tribal_government
	}
	1215.1.1 = { change_development_level = 6}
}
c_adthus = {
	1250.1.1 = {
		holder = clanlands_malcois_42
		liege = d_oios
		government = tribal_government
	}
}
c_olyris = {
	1250.1.1 = {
		holder = clanlands_malcois_42
		liege = d_oios
		government = tribal_government
	}
}
c_arkos = {
	1250.1.1 = {
		holder = clanlands_malcois_43
		liege = d_oios
		government = tribal_government
	}
}
d_cose = {
	1250.1.1 = {
		holder = clanlands_malcois_44
		liege = e_sjalvolki
		government = tribal_government
	}
	1215.1.1 = { change_development_level = 6}
}
c_amblana = {
	1250.1.1 = {
		holder = clanlands_malcois_45
		liege = d_cose
		government = tribal_government
	}
}
c_hymis = {
	1250.1.1 = {
		holder = clanlands_malcois_45
		liege = d_cose
		government = tribal_government
	}
}

d_diath = {
1215.1.1 = { change_development_level = 6}
}
c_helyma = {
	1250.1.1 = {
		holder = clanlands_malcois_46
		liege = d_cose
		government = tribal_government
	}
}
c_teympgive = {
	1250.1.1 = {
		holder = clanlands_malcois_46
		liege = d_cose
		government = tribal_government
	}
}
d_olafos = {
1215.1.1 = { change_development_level = 7}
}
c_arkiris = {
	1250.1.1 = {
		holder = clanlands_malcois_47
		liege = e_sjalvolki
		government = tribal_government
	}
}

d_dephoephladus = {
1215.1.1 = { change_development_level = 6}
}
c_gedrosris = {
	1250.1.1 = {
		holder = clanlands_malcois_47
		liege = e_sjalvolki
		government = tribal_government
	}
}
c_spagora = {
	1250.1.1 = {
		holder = clanlands_malcois_47
		liege = e_sjalvolki
		government = tribal_government
	}
}
d_anadres = {
	1250.1.1 = {
		holder = clanlands_malcois_48
		liege = e_sjalvolki
		government = tribal_government
	}
	1215.1.1 = { change_development_level = 5}
}
c_rhoum = {
	1250.1.1 = {
		holder = clanlands_malcois_49
		liege = d_anadres
		government = tribal_government
	}
}
c_lephnes = {
	1250.1.1 = {
		holder = clanlands_malcois_49
		liege = d_anadres
		government = tribal_government
	}
}
c_spase = {
	1250.1.1 = {
		holder = clanlands_malcois_50
		liege = d_anadres
		government = tribal_government
	}
}

d_anphon = {
1215.1.1 = { change_development_level = 8}
}
c_assion = {
	1250.1.1 = {
		holder = clanlands_malcois_51
		liege = d_anadres
		government = tribal_government
	}
}
c_emgieus = {
	1250.1.1 = {
		holder = clanlands_malcois_52
		liege = d_anadres
		government = tribal_government
	}
}
d_vroradi = {
	1250.1.1 = {
		holder = clanlands_malcois_53
		liege = e_sjalvolki
		government = tribal_government
	}
	1215.1.1 = { change_development_level = 9}
}
c_lekos = {
	1250.1.1 = {
		holder = clanlands_malcois_54
		liege = d_vroradi
		government = tribal_government
	}
}
c_paene = {
	1250.1.1 = {
		holder = clanlands_malcois_54
		liege = d_vroradi
		government = tribal_government
	}
}
d_grydi = {
1215.1.1 = { change_development_level = 7}
}
c_asai = {
	1250.1.1 = {
		holder = clanlands_malcois_55
		liege = d_vroradi
		government = tribal_government
	}
}






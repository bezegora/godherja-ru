##Mayikprollan and slave tribes
#Titles Generated at 2020/10/10 12:04:55 by mpjama
e_mayikprolollan = {
	1215.1.1 = { change_development_level = 8 }
	
	1215.1.1 = {
		holder = n_aversaria_1
		government = magocratic_government
	}
}
#Titles Generated at 2020/10/10 12:09:34 by mpjama
d_arth = {
	1249.1.1 = {
		holder = n_aversaria_2
	}
	1215.1.1 = { change_development_level = 1 }
}
d_geres = {
	1249.1.1 = {
		holder = n_aversaria_3
	}
	1215.1.1 = { change_development_level = 1 }
}
d_vredynai = {
	1249.1.1 = {
		holder = n_aversaria_4
	}
	1215.1.1 = { change_development_level = 2 }
}
c_ariacas = {
	1249.1.1 = {
		holder = n_aversaria_5
	}
}
d_peonopnes = {
	1249.1.1 = {
		holder = n_aversaria_6
	}
	1215.1.1 = { change_development_level = 1 }
}
d_thothous = {
	1249.1.1 = {
		holder = n_aversaria_7
	}
	1215.1.1 = { change_development_level = 0 }
}
d_oseinia = {
	1249.1.1 = {
		holder = n_aversaria_8
	}
}
d_cukirhaeus = {
	1249.1.1 = {
		holder = n_aversaria_9
	}
	1215.1.1 = { change_development_level = 1 }
}
#Titles Generated at 2020/10/10 12:30:38 by mpjama
c_annpia = {
	1251.1.1 = {
		holder = n_aversaria_10
		liege = d_oseinia
	}
	1215.1.1 = { change_development_level = 1 }
}
c_cones = {
	1251.1.1 = {
		holder = n_aversaria_11
		liege = d_oseinia
	}
}
c_aplleboni = {
	1251.1.1 = {
		holder = n_aversaria_12
		liege = d_oseinia
	}
}
#Titles Generated at 2020/10/10 12:35:41 by mpjama
c_coria = {
	1253.1.1 = {
		liege = d_peonopnes
	}
}
#Titles Generated at 2020/10/10 12:39:20 by mpjama
c_charea = {
	1253.1.1 = {
		holder = n_aversaria_14
	}
}
c_thephathus = {
	1253.1.1 = {
		holder = n_aversaria_15
	}
}
#Titles Generated at 2020/10/10 12:51:38 by mpjama
d_emiodi = {
	1249.1.1 =
{
government = magocratic_government
holder = n_aversaria_16
		liege = e_mayikprolollan
	}
	1215.1.1 = { change_development_level = 10 }
}
d_clamae = {
	1249.1.1 =
{
government = magocratic_government
holder = n_aversaria_17
		liege = e_mayikprolollan
	}
	1215.1.1 = { change_development_level = 2 }
}

c_atce = {
	1249.1.1 =
{
government = magocratic_government
holder = n_aversaria_1
}
}
c_drymethezoma = {
	1249.1.1 =
{
government = magocratic_government
holder = n_aversaria_1
}
}

d_tadi = {
	1249.1.1 =
{
government = magocratic_government
holder = n_aversaria_1
	}
	1215.1.1 = { change_development_level = 1 }
}
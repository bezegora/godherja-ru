#Harthath ibn sarradon

k_khenemhat = {
	1215.1.1 = {
		holder = sarradon_1
	}
}
k_ashkesk = {
	1215.1.1 = {
		holder = sarradon_1
	}
}
k_aglabolu = {
	1215.1.1 = {
		holder = sarradon_1
	}
}
k_lihyacca = {
	1215.1.1 = {
		holder = sarradon_1
	}
}
c_gharstir = {
	1215.1.1 = {
		holder = sarradon_1
	}
}
d_ghasab = {
	1215.1.1 = {
		holder = khenemhat_26
		liege = "k_khenemhat"
	}
	1215.1.1 = { change_development_level = 3 }
}

d_dedezkale = {
1215.1.1 = { change_development_level = 3 }
}

d_taliwahin = {
1215.1.1 = { change_development_level = 10 }
}
d_lawdarut = {
	1215.1.1 = {
		holder = khenemhat_26
		liege = "k_khenemhat"
	}
	1215.1.1 = { change_development_level = 3 }
}
k_mohrtag_al_badi = {
	1215.1.1 = {
		holder = blind_watcher_1
	}
	1215.1.1 = { change_development_level = 12 }
}
d_al_miwf = {
	1215.1.1 = {
		holder = khenemhat_20
		liege = "k_mohrtag_al_badi"		
	}
	1215.1.1 = { change_development_level = 9 }
}
d_ain_el_kezouar = {
	1215.1.1 = {
		holder = khenemhat_15
		liege = "k_khenemhat"
	}
	1215.1.1 = { change_development_level = 9 }
}
d_madada = {
	1215.1.1 = {
		holder = khenemhat_10
		liege = "k_mohrtag_al_badi"
	}
	1215.1.1 = { change_development_level = 6 }
}
d_melloutia = {
	1215.1.1 = {
		holder = khenemhat_5
		liege = "k_mohrtag_al_badi"
	}
	1215.1.1 = { change_development_level = 6 }
}
d_chenitour = {
	1215.1.1 = {
		holder = khenemhat_5
		liege = "k_mohrtag_al_badi"
	}
	1215.1.1 = { change_development_level = 8 }
}
d_hamchar = {
	1215.1.1 = {
		holder = khenemhat_1
		liege = "k_mohrtag_al_badi"
	}
	1215.1.1 = { change_development_level = 8 }
}
d_khekech = {
	1215.1.1 = {
		holder = khenemhat_1
		liege = "k_mohrtag_al_badi"
	}
	1215.1.1 = { change_development_level = 8 }
}
c_oujtat = {
	1215.1.1 = {
		holder = khenemhat_61
		liege = d_hamchar
	}
}
c_sedgourt = {
	1215.1.1 = {
		holder = khenemhat_61
		liege = d_hamchar
	}
}
c_taougane = {
	1215.1.1 = {
		holder = khenemhat_62
		liege = d_hamchar
	}
}
c_befra = {
	1215.1.1 = {
		holder = khenemhat_62
		liege = d_hamchar
	}
}

d_almilada = {
1215.1.1 = { change_development_level = 7 }
}

d_waziden = {
1215.1.1 = { change_development_level = 8 }
}

d_kemayeri = {
	1215.1.1 = {
		holder = khenemhat_28
		liege = "k_khenemhat"
	}
	1215.1.1 = { change_development_level = 4 }
}
d_almudir = {
	1215.1.1 = {
		holder = khenemhat_27
		liege = k_khenemhat
	}
	1215.1.1 = { change_development_level = 8 }
}
d_almilada = {
	1215.1.1 = {
		holder = khenemhat_27
		liege = k_khenemhat
	}
}
c_alibralqas = {
	1215.1.1 = {
		holder = khenemhat_58
		liege = d_almudir
	}
}
c_hudabam = {
	1215.1.1 = {
		holder = khenemhat_58
		liege = d_almudir
	}
}
c_rezvanshahr = {
	1215.1.1 = {
		holder = khenemhat_59
		liege = d_almudir
	}
}
c_sasaqaq = {
	1215.1.1 = {
		holder = khenemhat_59
		liege = d_almudir
	}
}
c_bustakish = {
	1215.1.1 = {
		holder = khenemhat_29
		liege = k_khenemhat
	}
}
c_limattin = {
	1215.1.1 = {
		holder = khenemhat_29
		liege = k_khenemhat
	}
}
c_baisit = {
	1215.1.1 = {
		holder = khenemhat_30
		liege = k_khenemhat
	}
}
c_khami = {
	1215.1.1 = {
		holder = khenemhat_30
		liege = k_khenemhat
	}
}
d_geyime = {
	1215.1.1 = {
		holder = khenemhat_31
		liege = k_khenemhat
	}
	1215.1.1 = { change_development_level = 7 }
}

d_bijaker = {
1215.1.1 = { change_development_level = 5 }
}
c_thuwacca = {
	1215.1.1 = {
		holder = khenemhat_31
		liege = k_khenemhat
	}
}
d_tall_falludariyah = {
	1215.1.1 = {
		holder = khenemhat_32
		liege = k_khenemhat
	}
	1215.1.1 = { change_development_level = 4 }
}

d_yasi = {
1215.1.1 = { change_development_level = 4 }
}

d_fourveh = {
1215.1.1 = { change_development_level = 4 }
}

d_gonatian = {
1215.1.1 = { change_development_level = 5 }
}

c_nasillah = {
	1215.1.1 = {
		holder = khenemhat_32
		liege = k_khenemhat
	}
}
c_faidiyah = {
	1215.1.1 = {
		holder = khenemhat_33
		liege = k_khenemhat
	}
}
c_tabutin = {
	1215.1.1 = {
		holder = khenemhat_33
		liege = k_khenemhat
	}
}
c_alfakuk = {
	1215.1.1 = {
		holder = khenemhat_34
		liege = k_khenemhat
	}
}
c_bebinnish = {
	1215.1.1 = {
		holder = khenemhat_34
		liege = k_khenemhat
	}
}
c_latiaqqez = {
	1215.1.1 = {
		holder = khenemhat_34
		liege = k_khenemhat
	}
}
c_medihat = {
	1215.1.1 = {
		holder = khenemhat_35
		liege = k_khenemhat
	}
}
c_harerran = {
	1215.1.1 = {
		holder = khenemhat_35
		liege = k_khenemhat
	}
}
d_sirghadani = {
	1215.1.1 = {
		holder = khenemhat_36
		liege = k_khenemhat
	}
	1215.1.1 = { change_development_level = 3 }
}
d_yada = {
	1215.1.1 = {
		holder = khenemhat_36
		liege = k_khenemhat
	}
	1215.1.1 = { change_development_level = 4 }
}
c_muqdabaid = {
	1215.1.1 = {
		holder = khenemhat_55
		liege = d_sirghadani
	}
}
c_qadayea = {
	1215.1.1 = {
		holder = khenemhat_55
		liege = d_sirghadani
	}
}
c_ar_rath = {
	1215.1.1 = {
		holder = khenemhat_56
		liege = d_sirghadani
	}
}
c_mehmeslu = {
	1215.1.1 = {
		holder = khenemhat_57
		liege = d_sirghadani
	}
}

d_almasab = {
1215.1.1 = { change_development_level = 8 }
}
d_dorousan = {
	1215.1.1 = {
		holder = khenemhat_40
		liege = k_khenemhat
	}
	1215.1.1 = { change_development_level = 6 }
}
c_disuwai = {
	1215.1.1 = {
		holder = sarradon_35
	}
}
d_qazvijarm = {
	1215.1.1 = {
		holder = khenemhat_41
		liege = k_khenemhat
	}
	1215.1.1 = { change_development_level = 10 }
}
c_jarane = {
	1215.1.1 = {
		holder = khenemhat_52
		liege = d_qazvijarm
	}
}
c_alqurrani = {
	1215.1.1 = {
		holder = khenemhat_53
		liege = d_qazvijarm
	}
}
c_al_khubigh = {
	1215.1.1 = {
		holder = khenemhat_42
		liege = k_khenemhat
	}
}
c_tall_iskaya = {
	1215.1.1 = {
		holder = khenemhat_42
		liege = k_khenemhat
	}
}
c_kemarasi = {
	1215.1.1 = {
		holder = khenemhat_43
		liege = k_khenemhat
	}
}
c_thibischa = {
	1215.1.1 = {
		holder = khenemhat_43
		liege = k_khenemhat
	}
}
d_safbleh = {
	1215.1.1 = {
		holder = khenemhat_44
		liege = k_khenemhat
	}
	1215.1.1 = { change_development_level = 12 }
}
d_al_kuhdad = {
	1215.1.1 = {
		holder = khenemhat_45
		liege = k_khenemhat
	}
	1215.1.1 = { change_development_level = 7 }
}
c_kudahhar = {
	1215.1.1 = {
		holder = khenemhat_45
		liege = k_khenemhat
	}
	1215.1.1 = { change_development_level = 18 }
}
c_mosufar = {
	1215.1.1 = {
		holder = khenemhat_54
		liege = k_khenemhat
	}
}
d_al_miqdayah = {
	1215.1.1 = {
		holder = khenemhat_46
		liege = k_khenemhat
	}
	1215.1.1 = { change_development_level = 6 }
}
d_thuwabha = {
	1215.1.1 = {
		holder = khenemhat_47
		liege = k_khenemhat
	}
	1215.1.1 = { change_development_level = 5 }
}
d_al_oyoodah = {
	1215.1.1 = {
		holder = khenemhat_47
		liege = k_khenemhat
	}
	1215.1.1 = { change_development_level = 5 }
}
d_alzarshut = {
	1215.1.1 = {
		holder = khenemhat_48
		liege = k_khenemhat
	}
	1215.1.1 = { change_development_level = 4 }
}
c_kagime = {
	1215.1.1 = {
		holder = khenemhat_49
		liege = k_khenemhat
	}
}
c_sakalla = {
	1215.1.1 = {
		holder = khenemhat_49
		liege = k_khenemhat
	}
}
c_alghairbah = {
	1215.1.1 = {
		holder = khenemhat_50
		liege = k_khenemhat
	}
}
c_bayisova = {
	1215.1.1 = {
		holder = khenemhat_50
		liege = k_khenemhat
	}
}
c_harmadiq = {
	1215.1.1 = {
		holder = khenemhat_51
		liege = k_khenemhat
	}
}
c_felakese = {
	1215.1.1 = {
		holder = khenemhat_51
		liege = k_khenemhat
	}
}
c_andasen = {
	1215.1.1 = {
		holder = khenemhat_60
		liege = k_mohrtag_al_badi
	}
}
c_gseiyada = {
	1215.1.1 = {
		holder = khenemhat_60
		liege = k_mohrtag_al_badi
	}
}
c_majaia = {
	1215.1.1 = {
		holder = khenemhat_60
		liege = k_mohrtag_al_badi
	}
}

#Forgotten Saint

k_almubid = {
	1215.1.1 = {
		holder = forgotten_1
		government = landed_order_government
	}
}
k_dhami = {
	1215.1.1 = {
		holder = forgotten_1
	}
}
c_as_sulatoorah = {
	1215.1.1 = {
		holder = forgotten_1
	}
}

c_doganbey = {
	1215.1.1 = {
		holder = forgotten_2
		liege = k_almubid
		government = landed_order_government
	}
}

c_saasazeh = {
	1215.1.1 = {
		holder = forgotten_3
		liege = k_almubid
		government = landed_order_government
	}
}

c_naqanzala = {
	1215.1.1 = {
		holder = forgotten_4
		liege = k_almubid		
		government = landed_order_government
	}
}

c_asiona = {
	1215.1.1 = {
		holder = forgotten_5
		liege = k_almubid		
		government = landed_order_government
	}
}

c_sayyishli = {
	1215.1.1 = {
		holder = forgotten_5
		liege = k_almubid		
	}
}

d_aljabish = {
	1215.1.1 = {
		holder = forgotten_6
		liege = k_almubid		
		government = landed_order_government
	}
	1215.1.1 = { change_development_level = 6 }
}

c_zakhodiyah = {
	1215.1.1 = {
		holder = forgotten_7
		liege = d_aljabish		
		government = landed_order_government
	}
}

c_mukabid = {
	1215.1.1 = {
		holder = forgotten_8
		liege = d_aljabish		
		government = landed_order_government
	}
}

d_qudsashniyah = {
	1215.1.1 = {
		government = landed_order_government 
		holder = forgotten_9
		liege = k_almubid		
	}
	1215.1.1 = { change_development_level = 11 }
}

d_albaybischa = {
1215.1.1 = { change_development_level = 5 }
}
d_al_ruheiniyas = {
	1215.1.1 = {
		holder = forgotten_10
		liege = k_almubid		
		government = landed_order_government
	}
	1215.1.1 = { change_development_level = 10 }
}

c_jabada = {
	1215.1.1 = {
		holder = forgotten_11
		liege = d_al_ruheiniyas		
		government = landed_order_government
	}
}

c_abu_kafta = {
	1215.1.1 = {
		government = theocracy_government	
		holder = forgotten_12
		liege = k_almubid		
	}
}

d_hajjarut = {
	1215.1.1 = {
		holder = forgotten_13
		liege = k_almubid		
		government = landed_order_government
	}
	1215.1.1 = { change_development_level = 10 }
}

d_amadaya = {
1215.1.1 = { change_development_level = 8 }
}

d_mahasrakh = {
	1215.1.1 = {
		holder = forgotten_13
		liege = k_almubid		
	}
	1215.1.1 = { change_development_level = 12 }
}

d_kafabulus = {
1215.1.1 = { change_development_level = 11 }
}

c_domamiyya = {
	1215.1.1 = {
		holder = forgotten_14
		liege = k_almubid		
		government = landed_order_government
	}
}

c_zabashli = {
	1215.1.1 = {	
		holder = forgotten_14
		liege = k_almubid		
	}
}

c_shirasht = {
	1215.1.1 = {	
		holder = forgotten_15
		liege = k_almubid		
		government = landed_order_government
	}
}

c_fahalla = {
	1215.1.1 = {	
		holder = forgotten_15
		liege = k_almubid		
	}
}

#Colonia


d_kamyaneh = {
	1215.1.1 = {
		holder = colonia_1
	}
	1215.1.1 = { change_development_level = 4 }
}

d_at_tash = {
1215.1.1 = { change_development_level = 3 }
}

d_hararwad = {
1215.1.1 = { change_development_level = 4 }
}
c_tallqush = {
	1215.1.1 = {
		government = republic_government
		holder = coloniavassal_1
		liege = d_kamyaneh
	}
}
c_dubamas = {
	1215.1.1 = {
		government = republic_government
		holder = coloniavassal_1
		liege = d_kamyaneh
	}
}
c_nasitba = {
	1215.1.1 = {
		holder = coloniavassal_2
		liege = d_kamyaneh
	}
}
c_quneibous = {
	1215.1.1 = {
		holder = coloniavassal_2
		liege = d_kamyaneh
	}
}

#Bedouin Dudes

d_hadirah = {
	1215.1.1 = {
		holder = marmaladid_1
	}
	1215.1.1 = { change_development_level = 2 }
}
d_babojarm = {
	1215.1.1 = {
		holder = marmaladid_2
	}
	1215.1.1 = { change_development_level = 2 }
}
d_miavaz = {
	1215.1.1 = {
		holder = marmaladid_2
	}
	1215.1.1 = { change_development_level = 2 }
}
d_baimar = {
	1215.1.1 = {
		holder = marmaladid_4
	}
	1215.1.1 = { change_development_level = 3 }
}






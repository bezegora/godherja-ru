﻿
##sanos

clanlands_malcois_1 = {
	name = Radoslav
	religion = "sjalvolki"
	culture = "daukeni"
	1219.4.12 = {
		birth = yes
	}
}

clanlands_malcois_2 = {
	name = Danilo
	religion = "sjalvolki"
	culture = "daukeni"
	1209.4.12 = {
		birth = yes
	}
}

clanlands_malcois_3 = {
	name = Asen
	religion = "sjalvolki"
	culture = "daukeni"
	1216.4.12 = {
		birth = yes
	}
}

clanlands_malcois_4 = {
	name = Ljudevit
	religion = "sjalvolki"
	culture = "daukeni"
	1222.4.12 = {
		birth = yes
	}
}

clanlands_malcois_5 = {
	name = Denkingen
	religion = "portenchant"
	culture = "mondecny"
	1214.4.12 = {
		birth = yes
	}
}

clanlands_malcois_6 = {
	name = Buhlingen
	religion = "portenchant"
	culture = "mondecny"
	1210.4.12 = {
		birth = yes
	}
}

clanlands_malcois_7 = {
	name = Balthasar
	religion = "portenchant"
	culture = "mondecny"
	1222.4.12 = {
		birth = yes
	}
}

clanlands_malcois_8 = {
	name = Arnntt
	religion = "portenchant"
	culture = "mondecny"
	1231.4.12 = {
		birth = yes
	}
}

###kallios

clanlands_malcois_9 = {
	name = Nikodim
	religion = "sjalvolki"
	culture = "daukeni"
	1230.4.12 = {
		birth = yes
	}
}

clanlands_malcois_10 = {
	name = Branimira
	female = yes 
	religion = "sjalvolki"
	culture = "daukeni"
	1224.4.12 = {
		birth = yes
	}
}

clanlands_malcois_11 = {
	name = Rastko
	religion = "sjalvolki"
	culture = "daukeni"
	1211.4.12 = {
		birth = yes
	}
}

clanlands_malcois_12 = {
	name = Tihomir
	religion = "sjalvolki"
	culture = "daukeni"
	1203.4.12 = {
		birth = yes
	}
}

clanlands_malcois_13 = {
	name = Elena
	female = yes 
	religion = "sjalvolki"
	culture = "daukeni"
	1212.4.12 = {
		birth = yes
	}
}

clanlands_malcois_14 = {
	name = Dmitar
	religion = "sjalvolki"
	culture = "daukeni"
	1210.4.12 = {
		birth = yes
	}
}

clanlands_malcois_15 = {
	name = Gavri
	religion = "sjalvolki"
	culture = "daukeni"
	1224.4.12 = {
		birth = yes
	}
}

clanlands_malcois_16 = {
	name = Lazar
	religion = "sjalvolki"
	culture = "daukeni"
	1223.4.12 = {
		birth = yes
	}
}

clanlands_malcois_17 = {
	name = Dragoslava
	female = yes
	religion = "sjalvolki"
	culture = "daukeni"
	1229.4.12 = {
		birth = yes
	}
}

clanlands_malcois_18 = {
	name = Freawine
	religion = "sjalvolki"
	culture = "tribocicai"
	1226.4.12 = {
		birth = yes
	}
}

clanlands_malcois_19 = {
	name = Ordulf
	religion = "sjalvolki"
	culture = "tribocicai"
	1218.4.12 = {
		birth = yes
	}
}

clanlands_malcois_20 = {
	name = Albrecht
	religion = "sjalvolki"
	culture = "tribocicai"
	1219.4.12 = {
		birth = yes
	}
}

clanlands_malcois_21 = {
	name = Hengest
	religion = "sjalvolki"
	culture = "tribocicai"
	1220.4.12 = {
		birth = yes
	}
}

clanlands_malcois_22 = {
	name = Marquard
	religion = "sjalvolki"
	culture = "tribocicai"
	1234.4.12 = {
		birth = yes
	}
}
clanlands_malcois_23 = {
	name = Gevert
	religion = "sjalvolki"
	culture = "tribocicai"
	1207.4.12 = {
		birth = yes
	}
}
clanlands_malcois_24 = {
	name = Frederuna
	female = yes
	religion = "sjalvolki"
	culture = "tribocicai"
	1200.4.12 = {
		birth = yes
	}
}
clanlands_malcois_25 = {
	name = Harthgate
	religion = "sjalvolki"
	culture = "tribocicai"
	1210.4.12 = {
		birth = yes
	}
}
clanlands_malcois_26 = {
	name = Irm
	female = yes
	religion = "sjalvolki"
	culture = "tribocicai"
	1230.4.12 = {
		birth = yes
	}
}
clanlands_malcois_27 = {
	name = Witikind
	religion = "sjalvolki"
	culture = "tribocicai"
	1220.4.12 = {
		birth = yes
	}
}
clanlands_malcois_28 = {
	name = Hinrik
	religion = "sjalvolki"
	culture = "tribocicai"
	1226.4.12 = {
		birth = yes
	}
}
clanlands_malcois_29 = {
	name = Kunigunde
	female = yes
	religion = "sjalvolki"
	culture = "tribocicai"
	1215.4.12 = {
		birth = yes
	}
}
clanlands_malcois_30 = {
	name = Warin
	religion = "sjalvolki"
	culture = "tribocicai"
	1212.4.12 = {
		birth = yes
	}
}

####

clanlands_malcois_31 = {
	name = Bertram
	religion = "sjalvolki"
	culture = "daukeni"
	1227.4.12 = {
		birth = yes
	}
}
clanlands_malcois_32 = {
	name = Giselher
	religion = "sjalvolki"
	culture = "daukeni"
	1228.4.12 = {
		birth = yes
	}
}
clanlands_malcois_33 = {
	name = Hermann
	religion = "sjalvolki"
	culture = "daukeni"
	1222.4.12 = {
		birth = yes
	}
}
clanlands_malcois_34 = {
	name = Abo
	religion = "sjalvolki"
	culture = "daukeni"
	1221.4.12 = {
		birth = yes
	}
}
clanlands_malcois_35 = {
	name = Giselmar
	religion = "sjalvolki"
	culture = "tribocicai"
	1234.4.12 = {
		birth = yes
	}
}
clanlands_malcois_36 = {
	name = Theodoric
	religion = "sjalvolki"
	culture = "tribocicai"
	1201.4.12 = {
		birth = yes
	}
}
clanlands_malcois_37 = {
	name = Hulderic
	religion = "sjalvolki"
	culture = "tribocicai"
	1204.4.12 = {
		birth = yes
	}
}
clanlands_malcois_38 = {
	name = Eilika
	female = yes
	religion = "sjalvolki"
	culture = "daukeni"
	1217.4.12 = {
		birth = yes
	}
}
clanlands_malcois_39 = {
	name = Ansgar
	religion = "sjalvolki"
	culture = "daukeni"
	1222.4.12 = {
		birth = yes
	}
}
clanlands_malcois_40 = {
	name = Reginlint
	religion = "sjalvolki"
	female = yes
	culture = "daukeni"
	1220.4.12 = {
		birth = yes
	}
}
clanlands_malcois_41 = {
	name = Meinwerk
	religion = "sjalvolki"
	culture = "daukeni"
	1225.4.12 = {
		birth = yes
	}
}
clanlands_malcois_42 = {
	name = Cissa
	religion = "sjalvolki"
	culture = "daukeni"
	1229.4.12 = {
		birth = yes
	}
}
clanlands_malcois_43 = {
	name = Mathilde
	female = yes
	religion = "sjalvolki"
	culture = "daukeni"
	1230.4.12 = {
		birth = yes
	}
}
clanlands_malcois_44 = {
	name = Engelbert
	religion = "sjalvolki"
	culture = "daukeni"
	1209.4.12 = {
		birth = yes
	}
}
clanlands_malcois_45 = {
	name = Immed
	religion = "sjalvolki"
	culture = "daukeni"
	1217.4.12 = {
		birth = yes
	}
}
clanlands_malcois_46 = {
	name = Asig
	religion = "sjalvolki"
	culture = "daukeni"
	1212.4.12 = {
		birth = yes
	}
}

clanlands_malcois_47 = {
	name = Wuldwine
	religion = "gethian"
	culture = "goans"
	1221.4.12 = {
		birth = yes
	}
}
clanlands_malcois_48 = {
	name = Angbo
	religion = "gethian"
	culture = "goans"
	1210.4.12 = {
		birth = yes
	}
}
clanlands_malcois_49 = {
	name = Sireng
	religion = "gethian"
	culture = "goans"
	1225.4.12 = {
		birth = yes
	}
}
clanlands_malcois_50 = {
	name = Misek
	religion = "gethian"
	culture = "goans"
	1228.4.12 = {
		birth = yes
	}
}
clanlands_malcois_51 = {
	name = Papo
	religion = "gethian"
	culture = "goans"
	1215.4.12 = {
		birth = yes
	}
}
clanlands_malcois_52 = {
	name = Yehang
	religion = "gethian"
	culture = "goans"
	1220.4.12 = {
		birth = yes
	}
}
clanlands_malcois_53 = {
	name = Tomang
	religion = "gethian"
	culture = "goans"
	1202.4.12 = {
		birth = yes
	}
}
clanlands_malcois_54 = {
	name = Khasuk
	female = yes
	religion = "gethian"
	culture = "goans"
	1229.4.12 = {
		birth = yes
	}
}
clanlands_malcois_55 = {
	name = Ladho
	religion = "gethian"
	culture = "goans"
	1224.4.12 = {
		birth = yes
	}
}
























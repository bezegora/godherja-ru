﻿
##sanos

history_misc_kathanouxa = {
	name = "Kathanouxa"
	religion = "kathanouxiac"
	culture = "daukeni"
	trait = magic_good_4
	trait = education_dead_magic_4
	trait = sadistic
	trait = arbitrary
	trait = arrogant
	sexuality = bisexual
	0.1.1 = {
		birth = yes
	}
	67.1.1 = {
		death = {
			death_reason = death_duel
			killer = marcher_20000
		}
	}
}
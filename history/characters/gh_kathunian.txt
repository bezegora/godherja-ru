﻿kathunian_1 = {
	#Child of the former ambasaddor from the far east to Aversaria and the last thing holding the diaspora together. P worried about his homeland probably being devoured by Fog.
	name = "Huaizhen"
                      dna = huaizhen
	religion = "fourteen_heavenly_methods"
	culture = "kathunian"
	father = kathunian_2
	1189.2.12 = {
		birth = yes
	}
}

kathunian_2 = { #former ambassador
	name = "Xi"
	religion = "fourteen_heavenly_methods"
	culture = "kathunian"
	1163.8.19 = {
		birth = yes
	}
		1240.9.1 = {
			death = yes
	}
}


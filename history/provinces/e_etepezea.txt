3348 = {
	culture = westernimperial
	religion = aversarinas_aautokrata
	holding = castle_holding

	0.1.1 = {
		special_building = thebloodyard
	}
}
3172 = {
	culture = westernimperial
	religion = aversarinas_aautokrata
	holding = castle_holding
}
3156 = {
	culture = westernimperial
	religion = blackhand
	holding = castle_holding
}
2816 = {
	culture = westernimperial
	religion = aversarinas_aautokrata
	holding = church_holding

	1200.1.1 = {
		special_building = thetowerwatch
	}
}

#Naupyrna
3088 = {
	culture = westernimperial
	religion = aversarinas_aautokrata
	holding = palatial_district_holding

	1200.1.1 = {
		buildings = {
			palace_03
			luxury_goods_03
			embassy_01
			fencing_School_02
		}
	}
}
3086 = {
	culture = westernimperial
	religion = aversarinas_aautokrata
	holding = maritime_district_holding

	1200.1.1 = {
		buildings = {
			maritime_district_02
			warehouse_02
			lighthouse_03
		}
	}
}
3152 = {
	culture = westernimperial
	religion = aversarinas_aautokrata
	holding = urban_district_holding

	1200.1.1 = {
		buildings = {
			urban_district_01
			wealthy_manors_01
			gymnasium_02
			amphitheatre_02
		}
	}
}
3221 = {
	culture = westernimperial
	religion = aversarinas_aautokrata
	holding = temple_district_holding

	1200.1.1 = {
		buildings = {
			temple_district_03
			clinic_02
			garden_02
		}
	}
}

##

3170 = {
	culture = westernimperial
	religion = aversarinas_aautokrata
	holding = castle_holding

	1253.11.17 = {
		special_building = elyssianpass
	}
}

3210 = {
	culture = westernimperial
	religion = aversarinas_aautokrata
	holding = castle_holding
}

2920 = {
	culture = westernimperial
	religion = aversarinas_aautokrata
	holding = castle_holding
	1240.1.1 = {
		culture = westernimperial
		religion = aversarian_pope
	}
}
3671 = {
	culture = westernimperial
	religion = aversarinas_aautokrata
	holding = castle_holding
	1240.1.1 = {
		culture = caemansi
		religion = sjalvolki
	}
}
2885 = {
	culture = westernimperial
	religion = aversarinas_aautokrata
	holding = castle_holding
	1240.1.1 = {
		culture = westernimperial
		religion = aversarian_axiaotheaism
	}
}
2882 = {
	culture = westernimperial
	religion = aversarinas_aautokrata
	holding = castle_holding

	671.3.30 = {
		special_building = imperialresort
	}
	1240.1.1 = {
		culture = westernimperial
		religion = aversarian_axiaotheaism
	}
}
##Oraispol
2937 = {
	culture = westernimperial
	religion = aversarinas_aautokrata
	holding = palatial_district_holding

	1200.1.1 = {
		buildings = {
			palace_04
			
			plaza_03
			arena_06
			luxury_goods_03
			plumbing_03
			embassy_02
		}
		special_building = stilto_katharoteras_01
	}
	1240.1.1 = {
		culture = westernimperial
		religion = aversarian_pope
	}
}
3265 = {
	culture = westernimperial
	religion = aversarinas_aautokrata
	holding = urban_district_holding

	1200.1.1 = {
		buildings = {
			urban_district_03
			peasant_housing_02
			wealthy_manors_02
		}
	}
	1240.1.1 = {
		culture = westernimperial
		religion = aversarian_pope
	}
}
2935 = {
	culture = westernimperial
	religion = aversarinas_aautokrata
	holding = maritime_district_holding

	1200.1.1 = {
		buildings = {
			maritime_district_04
			fishmarket_02
			lighthouse_04
			warehouse_02
		}
	}
	1240.1.1 = {
		culture = westernimperial
		religion = aversarian_pope
	}
}
2934 = {
	culture = westernimperial
	religion = aversarinas_aautokrata
	holding = trade_district_holding

	1200.1.1 = {
		buildings = {
			trade_district_03
			slave_market_02
			plaza_02
		}
	}
	1240.1.1 = {
		culture = westernimperial
		religion = aversarian_pope
	}
}
##Panticapaeum
3096 = {
	culture = westernimperial
	religion = aversarinas_aautokrata
	holding = castle_holding

	1200.1.1 = {
		buildings = {
			castle_02
		}
		special_building = drakavil_01
	}
	1240.1.1 = {
		culture = westernimperial
		religion = aversarian_pope
	}
}
3666 = {
	culture = westernimperial
	religion = aversarinas_aautokrata
	holding = castle_holding
}